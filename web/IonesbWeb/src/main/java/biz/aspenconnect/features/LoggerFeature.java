package biz.aspenconnect.features;

import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.FeatureContext;
import javax.ws.rs.ext.Provider;

import biz.aspenconnect.providers.AppLogger;

@Provider
public class LoggerFeature implements DynamicFeature {

	@Override
	public void configure(ResourceInfo ri, FeatureContext ctx) {
		AppLogger appLogger = ri.getResourceMethod().getAnnotation(AppLogger.class);
		if (appLogger == null) return;
		
//		System.out.println(appLogger.value());
	}

}
