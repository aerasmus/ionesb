package biz.aspenconnect.application.v1;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/api/v1")
public class OrderBookRestApplication extends Application {

}
