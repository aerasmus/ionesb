package biz.aspenconnect.service;

import biz.aspenconnect.TestClass;
import biz.aspenconnect.UserUtils;
import i1.esb.monitoring.beans.OrderBookServiceBean;
import biz.aspenconnect.exception.UnauthorizedException;
import biz.aspenconnect.providers.DTOName;
import biz.aspenconnect.utils.GeneralUtils;
import i1.esb.monitoring.dto.DataTransferObject;
import i1.esb.monitoring.pojo.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.sql.Timestamp;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Logger;


/**
 * Session Bean implementation class OrderBookRestSvc
 */
@Stateless
@LocalBean
// @RequestScoped
@Path( "/orderBook" )
@Api( value = "OrderBook << This is a GSCP related project that shows all the order books for a specific set of filter criteria." )
public class OrderBookRestSvc {

    private static String APP_VERSION = "V1.17.178.0922";


    public Logger logger = Logger.getLogger(new Object() {
    }.getClass().getName());

    @EJB
    OrderBookServiceBean orderBookServiceBean;

    GeneralUtils GU = new GeneralUtils();
    UserUtils UU = new UserUtils();

    //ToDo: To be removed after migration.
    TestClass TC = new TestClass();

    @Context
    SecurityContext sec;

    public OrderBookRestSvc() {
        super();
    }

    @Path( "/version" )
    @POST
    @Produces( MediaType.TEXT_PLAIN )
    @ApiOperation( value = "This operation is used to verify the version of the deployed services." )
    public Response version() {

        return Response.ok("OrderBook version :: " + APP_VERSION ).build();
    }

    @Path( "/list" )
    @POST
    @Consumes( MediaType.APPLICATION_JSON )
    @Produces( {MediaType.APPLICATION_JSON, "text/csv"} )
    @ApiOperation( value = "This operation is used to get a list of an entities' customers - SELECT DISTINCT" )
    public Response getOrderbookList(FilterOptions fo) throws Exception {

        logger.fine("OrderBookRestSvc DataTransferObject getOrderbookList(FilterOptions)");

//        if (!UU.isValidUser(sec))
//            return Response.ok(orderBookServiceBean.authenticationFailure(sec.getUserPrincipal().getName())).build();
//        else {
//            return Response.ok(orderBookServiceBean.getOrderBookList(fo)).build();
//        }
        return Response.ok("OrderBook version :: " + APP_VERSION ).build();
    }

    @Path( "/get/orders" )
    @POST
    @Consumes( {MediaType.APPLICATION_JSON} )
    @Produces( {MediaType.APPLICATION_JSON, "text/csv"} )
    public Response getOrdersByFilter(RqOrderbook rqOb) throws Exception {

        logger.info("OrderBookRestSvc DataTransferObject getOrdersByFilter(RqOrderbook)");

//        if (!UU.isValidUser(sec))
//            if (sec.getUserPrincipal() != null)
//                return Response.ok(orderBookServiceBean.authenticationFailure(sec.getUserPrincipal().getName())).build();
//            else
//                return Response.ok(orderBookServiceBean.authenticationFailure("Null")).build();
//        else {
//            DataTransferObject dto = orderBookServiceBean.getOrderBooksByFilter(rqOb);
//            return Response.ok(dto).build();
//        }
        return Response.ok("OrderBook version :: " + APP_VERSION ).build();
    }

    @DTOName(dtoTypeName="RsDtoOrderbook")
    @Path( "/dwn/orders" )
    @POST
    @Consumes( {MediaType.APPLICATION_JSON} )
    @Produces( {"text/csv"} )
    public Response dwnOrdersByFilter(RqOrderbook rqOb) throws Exception {

        logger.fine("dwnOrdersByFilter DataTransferObject dwnOrdersByFilter(RqOrderbook)");
        logger.fine(rqOb.toString());

//        return Response.ok(orderBookServiceBean.dwnOrderBooksByFilter(rqOb)).header("Content-Disposition", "attachment; filename=lets_test_it.csv").build();
        return Response.ok("OrderBook version :: " + APP_VERSION ).build();
    }

    @Path( "/get/init" )
    @POST
    @Consumes( MediaType.APPLICATION_JSON )
    @Produces( {MediaType.APPLICATION_JSON, "text/csv"} )
    @ApiOperation( value = "This operation is used to extract a list of all the status' for OrderBook. There is no filtering on this call." )
    public Response getOBInitValues(FilterOptions fo) throws Exception {

        logger.fine("OrderBookRestSvc DataTransferObject getOBInitValues() :: " + fo.toString());
        try {
            UU.isValidUser(sec);
            DataTransferObject obsDto = orderBookServiceBean.getOBInitConfig(fo);
            return Response.ok(obsDto).build();
        } catch (UnauthorizedException ue) {
            throw new UnauthorizedException(ue.getExceptionString());
        }
    }

    @Path( "/add/orderbook" )
    @POST
    @Consumes( MediaType.APPLICATION_JSON )
    @Produces( MediaType.APPLICATION_JSON )
    @ApiOperation( value = "This operation is used to add an entry to the orderbooks list (Will mostly be used for testing purposes)." )
    public Response addOrderBook() throws Exception { // List<OrderBookDto>

        logger.fine("OrderBookRestSvc List<OrderBook> addOrderBooks()");
//
//        try {
//            UU.isValidUser(sec);
//
//            try {
//                orderBookServiceBean.addOrderBook(odb);
//            } catch (Exception e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//            return Response.ok(odb).build();
//
//        } catch (UnauthorizedException ue) {
//            throw new UnauthorizedException(ue.getExceptionString());
//        }
        return Response.ok("OrderBook version :: " + APP_VERSION ).build();
    }

 }
