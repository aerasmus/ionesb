package biz.aspenconnect.exceptions;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class NotFoundExceptionMapper implements ExceptionMapper <NotFoundException> {
	@Override
	public Response toResponse(NotFoundException notFoundException) {
		return Response.
				status(Response.Status.INTERNAL_SERVER_ERROR).
				entity(new Error(ErrorCode.ENTITY_NOT_FOUND, notFoundException.getMessage(), notFoundException)).
				build();
	} // toResponse(NotFoundException)
}
