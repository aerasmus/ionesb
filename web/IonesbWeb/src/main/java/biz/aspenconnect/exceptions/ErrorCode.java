package biz.aspenconnect.exceptions;

public enum ErrorCode {
	
	UNKNOWN_ERROR (0, "Unknown Error"),
	ENTITY_NOT_FOUND (900, "Entity not found"),
	ENTITY_ALREADY_EXISTS (901, "Entity already exists"),
	AUTHENTICATION_ERROR(800, "Authentication Error"),
	AUTHORIZATION_ERROR(801, "Authorization Error");
	
	private int code;
	private String message;
	
	private ErrorCode(int code, String message) {
		this.code = code;
		this.message = message;
	} // ErrorCode(int, String)
	
	public int getCode() {
		return code;
	} // getCode
	
	public String getMessage() {
		return message;
	} // getMessage()
	
	public String toString() {
		return message;
	} // toString()

} // ErrorCode
