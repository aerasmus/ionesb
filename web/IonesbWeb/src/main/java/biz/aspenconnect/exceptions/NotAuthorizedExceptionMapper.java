package biz.aspenconnect.exceptions;

import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

//@Provider
@Produces(MediaType.APPLICATION_JSON)
public class NotAuthorizedExceptionMapper implements ExceptionMapper <NotAuthorizedException> {
	@Override
	public Response toResponse(NotAuthorizedException exception) {
		return Response.
				status(Response.Status.UNAUTHORIZED).
				entity(new Error(ErrorCode.AUTHORIZATION_ERROR, exception.getMessage(), exception)).
				build();
	} // toResponse(NotAuthorizedException)
} // NotAuthorizedExceptionMapper
