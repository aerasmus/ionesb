package biz.aspenconnect.exceptions;

import javax.ws.rs.Produces;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
@Produces(MediaType.APPLICATION_JSON)
public class GeneralExceptionMapper implements ExceptionMapper <Exception> {
	@Produces(MediaType.APPLICATION_JSON)
	@Override
	public Response toResponse(Exception exception) {
		return Response.
				status(Response.Status.INTERNAL_SERVER_ERROR).
				header(HttpHeaders.CONTENT_TYPE, "application/json").
				entity(new Error(ErrorCode.UNKNOWN_ERROR, exception.getMessage(), exception)).
				build();
	} // toResponse(EntityNotFoundException)
} // GeneralExceptionMapper
