package biz.aspenconnect.exceptions;

import javax.persistence.EntityNotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

//@Provider
public class EntityNotFoundMapper implements ExceptionMapper <EntityNotFoundException> {

	@Override
	public Response toResponse(EntityNotFoundException exception) {
		return Response.status(Response.Status.NOT_FOUND)
				.entity(new Error(ErrorCode.ENTITY_NOT_FOUND, exception.getMessage(), exception))
				.build();
	} // toResponse(EntityNotFoundException)

} // EntityNotFoundMapper
