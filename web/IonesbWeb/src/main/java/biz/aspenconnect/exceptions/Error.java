package biz.aspenconnect.exceptions;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Error")
public class Error {
	
	@XmlElement(name="code")
	private int code;
	
	@XmlElement(name="dmsg")
	private String dMsg;

	@XmlElement(name="umsg")
	private String uMsg;
	
	@XmlElement(name="etype")
	private String eType;
	
	public Error() {
	} // Error()
	
	public Error(ErrorCode errorCode) {
		this.code = errorCode.getCode();
		this.uMsg = errorCode.getMessage();
	} // Error(ErrorCode)

	public Error(ErrorCode errorCode, String dMsg) {
		this(errorCode);
		this.setdMsg(dMsg);
	} // Error(ErrorCode)

	public Error(ErrorCode errorCode, String dMsg, Exception e) {
		this(errorCode, dMsg);
		this.seteType(e.getClass().getName());
	} // Error(ErrorCode)
	
	
	public int getCode() {
		return code;
	} // getCode()
	
	public void setCode(int code) {
		this.code = code;
	} // setCode(int)
	
	public String getdMsg() {
		return dMsg;
	} // getdMsg()
	
	public void setdMsg(String dMsg) {
		this.dMsg = dMsg;
	} // setdMsg(String)
	
	public String getuMsg() {
		return uMsg;
	} // getuMsg()
	
	public void setuMsg(String uMsg) {
		this.uMsg = uMsg;
	} // setuMsg(String)

	public String geteType() {
		return eType;
	} // geteType()

	public void seteType(String eType) {
		this.eType = eType;
	} // seteType(String)

	
	
} // Error
