package biz.aspenconnect.providers.exception;

import biz.aspenconnect.exception.ForbiddenException;
import biz.aspenconnect.exception.UnauthorizedException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ForbiddenProvider implements ExceptionMapper<ForbiddenException>{

    @Override
    public Response toResponse(ForbiddenException e) {
        return Response.status(Response.Status.UNAUTHORIZED).type(MediaType.TEXT_PLAIN).entity(e.getExceptionString()).build();
    }
}
