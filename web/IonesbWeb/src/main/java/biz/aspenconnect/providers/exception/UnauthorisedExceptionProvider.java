package biz.aspenconnect.providers.exception;

import biz.aspenconnect.exception.UnauthorizedException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class UnauthorisedExceptionProvider implements ExceptionMapper<UnauthorizedException>{

    @Override
    public Response toResponse(UnauthorizedException e) {

        return Response.status(Response.Status.UNAUTHORIZED).type(MediaType.TEXT_PLAIN).entity("" + Response.Status.UNAUTHORIZED.getStatusCode() + " " + Response.Status.UNAUTHORIZED + ": " + e.getExceptionString()).build();
    }
}
