package biz.aspenconnect.providers.exception;

import biz.aspenconnect.exception.DeprecatedException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class DeprecationExceptionProvider implements ExceptionMapper<DeprecatedException>{

    @Override
    public Response toResponse(DeprecatedException e) {
        return Response.status(Response.Status.RESET_CONTENT).type(MediaType.TEXT_PLAIN).entity(e.getExceptionString()).build();
    }
}
