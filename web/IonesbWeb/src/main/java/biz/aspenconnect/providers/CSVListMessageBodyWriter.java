package biz.aspenconnect.providers;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

import biz.aspenconnect.utils.GeneralUtils;

@Provider
@Produces("text/csv")
public class CSVListMessageBodyWriter implements MessageBodyWriter<List<RsDtoOrderbook>> {

	GeneralUtils GU = new GeneralUtils();

	private RsDtoOrderbook createOrderBookDto(Constructor<?> ctor) {
		try {
			RsDtoOrderbook u = (RsDtoOrderbook) ctor.newInstance(); //was null
			return u;
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		// If something went wrong, return null so this line can be skipped
		return null;

	} // createDto()

	private String getDtoName(Annotation[] annotations) {
//		System.out.println("getDTOName");
		for (Annotation annotation : annotations) {
			if (annotation instanceof DTOName) {
//				System.out.println("getDTOName :: " + ((DTOName) annotation).dtoTypeName());
				return ((DTOName) annotation).dtoTypeName();
			}
		}
		return null;
	}

	@Override
	public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
		return true;
	}

	@Override
	public long getSize(List<RsDtoOrderbook> dto, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
		// deprecated by JAX-RS 2.0 and ignored by runtime
		return 0;
	}

	public void writeTo(List<RsDtoOrderbook> dto, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, Object> httpHeaders, OutputStream out) throws IOException, WebApplicationException {

		try {

			Class<?> clazz = null;
			Constructor<?> ctor = null;
			
			try {
				clazz = Class.forName(getDtoName(annotations));
				ctor = clazz.getConstructor();
			} catch (Exception e) {
				e.printStackTrace();
			}

			Writer writer = new PrintWriter(out);

//			System.out.println("CSVListMessageBodyWriter :: NumRecords=" + dto.size());

			RsDtoOrderbook odbInst = createOrderBookDto(ctor);

			Date expDte = new Date();
			writer.write("OrderBook Export ("+ GU.formatDate(expDte,"yyyy-MM-dd HH:MM:SS") + ")" + "\n");
			writer.write(odbInst.getHeaders() + "\n");

			for (RsDtoOrderbook o : dto) {
				writer.write(o.toString() + "\n");
			}
			writer.write("Records: " + dto.size() + "\n");
			writer.flush();
			writer.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
