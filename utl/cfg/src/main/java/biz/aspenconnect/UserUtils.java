package biz.aspenconnect;

import biz.aspenconnect.exception.UnauthorizedException;

import javax.ws.rs.core.SecurityContext;
import java.util.logging.Logger;

/**
 * Created by bere0 on 2017-06-21.
 */
public class UserUtils {

    public Logger logger = Logger.getLogger(new Object() {
    }.getClass().getName());

    public UserUtils() {
        super();
    }

    public boolean isValidUser(SecurityContext sec) throws UnauthorizedException{

        try {
            if (sec.getUserPrincipal() != null) {
//                logger.fine("isValidUser :: User Credentials :: getAuthenticationScheme :: " + sec.getAuthenticationScheme() + " || getUserPrincipal.getName :: " + sec.getUserPrincipal().getName() + " || isSecure :: " + sec.isSecure());
                return true;
            } else {
                logger.fine("isValidUser :: User Credentials :: Credentials is Null");
            return false;
            }
        } catch (NullPointerException npe) {
            logger.fine("isValidUser :: User Credentials :: Credentials is Null");
//            return true;
            return false;
        }

    }
}
