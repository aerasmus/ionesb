package biz.aspenconnect;

import biz.aspenconnect.orderbook.dto.OrderBookDto;
import biz.aspenconnect.orderbook.dto.types.AgiTypeDto;
import biz.aspenconnect.orderbook.dto.types.CustTypeDto;
import biz.aspenconnect.orderbook.dto.types.ItemTypeDto;
import biz.aspenconnect.orderbook.dto.types.OrderTypeDto;
import biz.aspenconnect.utils.GeneralUtils;

import java.sql.Timestamp;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static java.lang.String.format;

/**
 * Created by bere0 on 2017-06-13.
 */
public class TestClass {

    GeneralUtils GU = new GeneralUtils();

    private static String iLp = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

    private static String[] cust = {"700300", "700301", "702300", "703201", "703400", "703600", "800100", "800120", "AUASP", "AUDUK", "CNASP", "HGASP", "HKGASP", "IDASP", "JPASP", "KRASP", "NZASP", "SGASP", "SGPASP", "THASP", "THPASP", "AANTG", "VNASP", "ASPCHN", "ASPHK1", "ASPHK2", "ASPHK3", "ASPIDN", "ASPIND", "ASPKOR", "ASPSG1", "ASPSG2", "ASPSG3", "ASPSLK", "ASPTH1", "ASPTHA", "SGHUB", "VNMASP"};
    private static String[] ctry = {"AU", "GE", "UK", "US", "ZA", "BZ", "GH", "NZ", "NI", "MA", "KQ"};
    private static String[] crcy = {"AUD", "EAU", "BPD", "USD", "ZAR"};
    private static String[] status = {"Planned", "Firm Planned", "Confirmed", "Received", "In Transit", "Closed", "Deleted", "Blocked", "Approved"};

    private static String[] aplha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");


    private String iL = GU.capitalizeString("Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut");
    private String[] names = iL.split(" ");

    public TestClass() { super(); }

    public OrderBookDto buildDummyOrderBookDto() { // List<OrderBookDto>
        System.out.println(" >>> buildDummyOrderBookDto >>> 1");
        OrderBookDto odb= new OrderBookDto();;

        Date uDate = new Date();

        String Chr1 = aplha[0 + (int) (Math.random() * 26)];
        String Chr2 = aplha[0 + (int) (Math.random() * 26)] + aplha[0 + (int) (Math.random() * 26)];
        String Chr3 = aplha[0 + (int) (Math.random() * 26)] + aplha[0 + (int) (Math.random() * 26)] + aplha[0 + (int) (Math.random() * 26)];

        odb.setDtoSysOrderbookPk(0L);

        odb.setDtoAspen(genOdbGetDtoAspen(odb.getDtoAspen(), uDate, Chr1, Chr3));           // Aspen Details
        odb.setDtoCustomer(genOdbGetDtoCustomer(odb.getDtoCustomer(), Chr1, Chr2, Chr3));   //Customer
        odb.setDtoItems(genOdbGetDtoItems(odb.getDtoItems(), Chr1, Chr2, Chr3));          //Items
        odb.setDtoOrder(genOdbGetDtoItems(odb.getDtoOrder(), uDate, Chr1, Chr3)); //Order

        odb.setDtoSysLasttoucheduser(Chr2 + Chr3 + GU.leadingZeros(format("%s", (int) Math.random() * 999), 3));
        odb.setDtoSysLasttouched(new Timestamp(uDate.getTime()));
        System.out.println(" >>> buildDummyOrderBookDto >>> 2");
        return odb;
    }

    private OrderTypeDto genOdbGetDtoItems(OrderTypeDto otd, Date uDate, String chr1, String chr3) {
        otd.setDtoOrdCustomerPoLine(10L);
        otd.setDtoOrdFpaReference(chr3 + GU.leadingZeros("" + (int) (Math.random() * 99999), 7) + chr1);
        otd.setDtoOrdRevisedCorPlanDelDate(new Date(uDate.getTime() + TimeUnit.DAYS.toMillis((int) (Math.random() * 365))));
        otd.setDtoOrdSoCurrency(aplha[0 + (int) (Math.random() * 26)] + aplha[0 + (int) (Math.random() * 26)] + aplha[0 + (int) (Math.random() * 26)]);
        otd.setDtoOrdSoDate(new Date(uDate.getTime() + TimeUnit.DAYS.toMillis((int) (Math.random() * 365))));
        otd.setDtoOrdSoLine(10L);
        if ((Math.random() * 99999) > 70000)
            otd.setDtoOrdSoNumb(chr3 + GU.leadingZeros("" + (int) (Math.random() * 99999), 7) + chr1);
        otd.setDtoOrdSoPlannedDeliveryDate(new Date(uDate.getTime() + TimeUnit.DAYS.toMillis((int) (Math.random() * 365))));
        otd.setDtoOrdSoPrice(10D);
        otd.setDtoOrdSoQuantity(10D);
        otd.setDtoOrdSoValue(1234.01D);

        return otd;
    }

    private ItemTypeDto genOdbGetDtoItems(ItemTypeDto itd, String chr1, String chr2, String chr3) {
        itd.setDtoItmAgiItemCode(chr3 + GU.leadingZeros("" + (int) (Math.random() * 99999), 7) + chr1);
        itd.setDtoItmBrandDesc(GU.getSubstring(iLp, 47));
        itd.setDtoItmBrandNumb(chr3 + GU.leadingZeros("" + (int) (Math.random() * 99999), 7) + chr1);
        itd.setDtoItmItemDesc(GU.getSubstring(iLp, 47));
        itd.setDtoItmItemGroupDesc(GU.getSubstring(iLp, 47));
        itd.setDtoItmItemGroupNumb(chr3 + GU.leadingZeros("" + (int) (Math.random() * 99999), 7) + chr1);
        itd.setDtoItmItemTimeFenceDays(GU.getRandomLong(0, 365));
        itd.setDtoItmRhSignalCode(chr1 + chr2);
        itd.setDtoItmSoSkiiCode(chr3 + GU.leadingZeros("" + (int) (Math.random() * 99999), 7) + chr1);
        itd.setDtoItmSupplierCode(chr1 + chr2 + chr3);
        itd.setDtoItmSupplierDesc(GU.getSubstring(iLp, 48));
        itd.setDtoItmSupplierItemCode(chr3 + GU.leadingZeros("" + (int) (Math.random() * 9999999), 7) + chr1);
        itd.setDtoItmTimeLeadTotal(GU.getRandomLong(0, 365));
        itd.setDtoItmTimeQaSafety(GU.getRandomLong(0, 365));

        return itd;
    }

    private CustTypeDto genOdbGetDtoCustomer(CustTypeDto ctd, String chr1, String chr2, String chr3) {

        ctd.setDtoCusBusinesUnitDesc(GU.getSubstring(iLp, 11));
        ctd.setDtoCusBusinesUnitNumb(chr3 + GU.leadingZeros("" + (int) (Math.random() * 99), 2) + chr1);
        String cust_code = cust[(int) ((Math.random() * (cust.length - 1)) + 1)];
        ctd.setDtoCusCustomerCode(cust_code);
        ctd.setDtoCusCustomerName(GU.getSubstring(iLp, 48));
        ctd.setDtoCusCustomerNumber(cust_code);
        ctd.setDtoCusEndMarketCustCode(chr3 + GU.leadingZeros("" + (int) (Math.random() * 99999), 7) + chr1);
        ctd.setDtoCusEndMarketCustCtry(chr3);
        ctd.setDtoCusEndMarketCustName(GU.getSubstring(iLp, 64));
        ctd.setDtoCusPoNumb(chr2 + GU.leadingZeros("" + (int) (Math.random() * 99999), 7) + chr2);

        return ctd;
    }

    private AgiTypeDto genOdbGetDtoAspen(AgiTypeDto atd, Date uDate, String chr1, String chr3) {

        atd.setDtoAgiCluster(GU.getSubstring(iLp, 24));
        atd.setDtoAgiContact(GU.getSubstring(iL, 48));
        atd.setDtoAgiContactEmail((GU.getSubstring(iLp, 12) + "@" + GU.getSubstring(iLp, 10) + "." + GU.getSubstring(iLp, 3)).replaceAll("_", "").replaceAll(" ", "").toLowerCase());
        atd.setDtoAgiCorNumb(GU.getRandomLong(10000000L, 99999999L));
        atd.setDtoAgiCorStatus("" + (int) ((Math.random() * 11) + 1));
        atd.setDtoAgiExSiteDate(new Date(uDate.getTime() + TimeUnit.DAYS.toMillis((int) (Math.random() * 365))));
        atd.setDtoAgiInvoiceNumb(chr3 + GU.leadingZeros("" + (int) (Math.random() * 99), 3) + chr1);
        atd.setDtoAgiInvoiceType(chr3.toUpperCase());
        atd.setDtoAgiOriginalDeliveryDate(new Date(uDate.getTime() + TimeUnit.DAYS.toMillis((int) (Math.random() * 365))));
        atd.setDtoAgiPlannedDeliveryDate(new Date(uDate.getTime() + TimeUnit.DAYS.toMillis((int) (Math.random() * 365))));
        atd.setDtoAgiPlannedQuantity(GU.getRandomLong(0, 365).doubleValue());
        atd.setDtoAgiRecognisedQuantity(GU.getRandomLong(0, 365).doubleValue());
        atd.setDtoAgiRevisedExSiteDate(new Date(uDate.getTime() + TimeUnit.DAYS.toMillis((int) (Math.random() * 365))));
        atd.setDtoAgiVarianceDeliveryDate(GU.getRandomLong(0, 365).doubleValue());
        atd.setDtoAgiVariancePlannedQty(GU.getRandomLong(0, 365).doubleValue());

        return atd;
    }
}
