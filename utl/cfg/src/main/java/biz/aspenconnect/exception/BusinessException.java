package biz.aspenconnect.exception;

/**
 * Created by bere0 on 2017-06-27.
 */
public class BusinessException extends Exception {

    private String exceptionString;

    public BusinessException(String exceptionString) {
        super();
        this.exceptionString = exceptionString;
    }

    public String getExceptionString() {
        return exceptionString;
    }

    public void setExceptionString(String exceptionString) {
        this.exceptionString = exceptionString;
    }
}
