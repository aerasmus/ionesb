package biz.aspenconnect.exception;

public class UnauthorizedException extends Exception {

    private String exceptionString;

    public UnauthorizedException(String exceptionString) {
        super();
        this.exceptionString = exceptionString;
    }

    public String getExceptionString() {
        return exceptionString;
    }

    public void setExceptionString(String exceptionString) {
        this.exceptionString = exceptionString;
    }
}
