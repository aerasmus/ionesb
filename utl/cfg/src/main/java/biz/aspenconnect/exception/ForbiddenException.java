package biz.aspenconnect.exception;

public class ForbiddenException extends Exception {

    private String exceptionString;

    public ForbiddenException(String exceptionString) {
        super();
        this.exceptionString = exceptionString;
    }

    public String getExceptionString() {
        return exceptionString;
    }

    public void setExceptionString(String exceptionString) {
        this.exceptionString = exceptionString;
    }
}
