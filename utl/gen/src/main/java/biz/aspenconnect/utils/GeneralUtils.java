package biz.aspenconnect.utils;

import javax.xml.datatype.XMLGregorianCalendar;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static java.lang.String.valueOf;

/**
 * Created by bere0 on 2017-05-30.
 */
public class GeneralUtils {

    public GeneralUtils() {
        super();
    }

    public static String capitalizeString(String string) {
        char[] chars = string.toLowerCase().toCharArray();
        boolean found = false;
        for (int i = 0; i < chars.length; i++) {
            if (!found && Character.isLetter(chars[i])) {
                chars[i] = Character.toUpperCase(chars[i]);
                found = true;
            } else if (Character.isWhitespace(chars[i]) || chars[i] == '.' || chars[i] == '\'') {
                // You can add other chars here
                found = false;
            }
        }
        return valueOf(chars);
    }

    public Date fromGregorianToUtilDate(XMLGregorianCalendar gDate){
        return new Date((gDate.getYear() - 1900),(gDate.getMonth() - 1),gDate.getDay());
    }

    public String formatDate(Date fDate) {
        return formatDate(fDate, "yyyy-MM-dd");
    }

    public String formatDate(Date fDate, String pattern) {
        String formattedDate = "";
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);

        if (fDate != null) {
            formattedDate = formatter.format(fDate);
        }

        return formattedDate;
    }

    public Long getTimeDiffInDays(Date targetDate) {

        Calendar gCTarget = new GregorianCalendar();
        gCTarget.setTime(targetDate);

        Calendar gCNow = new GregorianCalendar();
        gCNow.setTime(new Date());

        Long TimeDiffInDays = 0L;
        if(gCTarget.getTimeInMillis() != gCNow.getTimeInMillis())
            TimeDiffInDays = (gCTarget.getTimeInMillis() - gCNow.getTimeInMillis()) / (1000 * 60 * 60 * 24);

        return TimeDiffInDays;
    }

    public static String leadingZeros(String fld, int len) {
        return (fld.length() < len ? getZeros(len - fld.length()) + fld : fld);
    }

    private static String getZeros(Integer num) {
        String out = "";
        for (int i = 0; i < num; i++) {
            out += "0";
        }
        return out;
    }

    public Long getRandomLong(long leftLimit, long rightLimit) {
        return leftLimit + (long) (Math.random() * (rightLimit - leftLimit));
    }

    public String getSubstring(String iLp, int len) {
        String tStr = iLp.substring((int) (Math.random() * len), len + (int) (Math.random() * len));
        return (tStr.length() > len ? tStr.substring(0, len - 1) : tStr);
    }

}
