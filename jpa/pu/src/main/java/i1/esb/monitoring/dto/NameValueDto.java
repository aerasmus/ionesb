package i1.esb.monitoring.dto;

import javax.xml.bind.annotation.XmlElement;

public class NameValueDto {
	
	@XmlElement(name="name")
	private String name;
	
	@XmlElement(name="value")
	private String value;

	public NameValueDto() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	
	

}

