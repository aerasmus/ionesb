package i1.esb.monitoring.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

//ToDo: 1. removae JPA annotations
//	2. Add jaxb annotations
// 3. create constructor for both model and dto - takes other is input 
// 4. change scalar types to object types
// 5. 

@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlRootElement(name = "customers")

public class CustomerDto implements Serializable {
	
	public CustomerDto(){
		super();
	}
	
	private static final long serialVersionUID = 1L;
	
	@XmlElement(name = "customerName")
	private String customer;
	
	@XmlElement(name = "customerNode")
	private String customerNode;

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getCustomerNode() {
		return customerNode;
	}

	public void setCustomerNode(String customerNode) {
		this.customerNode = customerNode;
	}


}
