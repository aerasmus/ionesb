package i1.esb.monitoring.pojo.types;

import biz.aspenconnect.orderbook.dto.types.ItemTypeDto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlRootElement(name = "Items")
public class RsDtoItm implements Serializable {

    private static final long serialVersionUID = 1L;

    @XmlElement(name = "AgiItemCode")
    private String dtoItmAgiItemCode;
    @XmlElement(name = "BrandDesc")
    private String dtoItmBrandDesc;
    @XmlElement(name = "BrandNumb")
    private String dtoItmBrandNumb;
    @XmlElement(name = "ItemDesc")
    private String dtoItmItemDesc;
    @XmlElement(name = "ItemGroupDesc")
    private String dtoItmItemGroupDesc;
    @XmlElement(name = "ItemGroupNumb")
    private String dtoItmItemGroupNumb;
    @XmlElement(name = "ItemTimeFenceDays")
    private Long dtoItmItemTimeFenceDays;
    @XmlElement(name = "RhSignalCode")
    private String dtoItmRhSignalCode;
    @XmlElement(name = "SoSkiiCode")
    private String dtoItmSoSkiiCode;
    @XmlElement(name = "SupplierCode")
    private String dtoItmSupplierCode;
    @XmlElement(name = "SupplierDesc")
    private String dtoItmSupplierDesc;
    @XmlElement(name = "SupplierItemCode")
    private String dtoItmSupplierItemCode;
    @XmlElement(name = "TimeLeadTotal")
    private Long dtoItmTimeLeadTotal;
    @XmlElement(name = "TimeQaSafety")
    private Long dtoItmTimeQaSafety;

    public RsDtoItm(ItemTypeDto odbDtoItm) {
        this.dtoItmAgiItemCode = odbDtoItm.getDtoItmAgiItemCode();
        this.dtoItmBrandDesc = odbDtoItm.getDtoItmBrandDesc();
        this.dtoItmBrandNumb = odbDtoItm.getDtoItmBrandNumb();
        this.dtoItmItemDesc = odbDtoItm.getDtoItmItemDesc();
        this.dtoItmItemGroupDesc = odbDtoItm.getDtoItmItemGroupDesc();
        this.dtoItmItemGroupNumb = odbDtoItm.getDtoItmItemGroupNumb();
        this.dtoItmItemTimeFenceDays = odbDtoItm.getDtoItmItemTimeFenceDays();
        this.dtoItmRhSignalCode = odbDtoItm.getDtoItmRhSignalCode();
        this.dtoItmSoSkiiCode = odbDtoItm.getDtoItmSoSkiiCode();
        this.dtoItmSupplierCode = odbDtoItm.getDtoItmSupplierCode();
        this.dtoItmSupplierDesc = odbDtoItm.getDtoItmSupplierDesc();
        this.dtoItmSupplierItemCode = odbDtoItm.getDtoItmSupplierItemCode();
        this.dtoItmTimeLeadTotal = odbDtoItm.getDtoItmTimeLeadTotal();
        this.dtoItmTimeQaSafety = odbDtoItm.getDtoItmTimeQaSafety();
    }

    @XmlElement(name = "AgiItemCode")
    public String getDtoItmAgiItemCode() {
        return dtoItmAgiItemCode;
    }

    @XmlElement(name = "BrandDesc")
    public String getDtoItmBrandDesc() {
        return dtoItmBrandDesc;
    }

    @XmlElement(name = "BrandNumb")
    public String getDtoItmBrandNumb() {
        return dtoItmBrandNumb;
    }

    @XmlElement(name = "ItemDesc")
    public String getDtoItmItemDesc() {
        return dtoItmItemDesc;
    }

    @XmlElement(name = "ItemGroupDesc")
    public String getDtoItmItemGroupDesc() {
        return dtoItmItemGroupDesc;
    }

    @XmlElement(name = "ItemGroupNumb")
    public String getDtoItmItemGroupNumb() {
        return dtoItmItemGroupNumb;
    }

    @XmlElement(name = "ItemTimeFenceDays")
    public Long getDtoItmItemTimeFenceDays() {
        return dtoItmItemTimeFenceDays;
    }

    @XmlElement(name = "RhSignalCode")
    public String getDtoItmRhSignalCode() {
        return dtoItmRhSignalCode;
    }

    @XmlElement(name = "SoSkiiCode")
    public String getDtoItmSoSkiiCode() {
        return dtoItmSoSkiiCode;
    }

    @XmlElement(name = "SupplierCode")
    public String getDtoItmSupplierCode() {
        return dtoItmSupplierCode;
    }

    @XmlElement(name = "SupplierDesc")
    public String getDtoItmSupplierDesc() {
        return dtoItmSupplierDesc;
    }

    @XmlElement(name = "SupplierItemCode")
    public String getDtoItmSupplierItemCode() {
        return dtoItmSupplierItemCode;
    }

    @XmlElement(name = "TimeLeadTotal")
    public Long getDtoItmTimeLeadTotal() {
        return dtoItmTimeLeadTotal;
    }

    @XmlElement(name = "TimeQaSafety")
    public Long getDtoItmTimeQaSafety() {
        return dtoItmTimeQaSafety;
    }

    @Override
    public String toString() {
        return "RsDtoItm{" +
                "dtoItmAgiItemCode='" + dtoItmAgiItemCode + '\'' +
                ", dtoItmBrandDesc='" + dtoItmBrandDesc + '\'' +
                ", dtoItmBrandNumb='" + dtoItmBrandNumb + '\'' +
                ", dtoItmItemDesc='" + dtoItmItemDesc + '\'' +
                ", dtoItmItemGroupDesc='" + dtoItmItemGroupDesc + '\'' +
                ", dtoItmItemGroupNumb='" + dtoItmItemGroupNumb + '\'' +
                ", dtoItmItemTimeFenceDays=" + dtoItmItemTimeFenceDays +
                ", dtoItmRhSignalCode='" + dtoItmRhSignalCode + '\'' +
                ", dtoItmSoSkiiCode='" + dtoItmSoSkiiCode + '\'' +
                ", dtoItmSupplierCode='" + dtoItmSupplierCode + '\'' +
                ", dtoItmSupplierDesc='" + dtoItmSupplierDesc + '\'' +
                ", dtoItmSupplierItemCode='" + dtoItmSupplierItemCode + '\'' +
                ", dtoItmTimeLeadTotal=" + dtoItmTimeLeadTotal +
                ", dtoItmTimeQaSafety=" + dtoItmTimeQaSafety +
                '}';
    }
}
