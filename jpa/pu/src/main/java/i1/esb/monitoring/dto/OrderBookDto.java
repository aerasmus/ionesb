package i1.esb.monitoring.dto;

import biz.aspenconnect.orderbook.dto.types.*;
import biz.aspenconnect.orderbook.model.OrderBook;
import biz.aspenconnect.orderbook.model.OrderBookV1;
import biz.aspenconnect.orderbook.pojo.OrderBookQue;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.sql.Timestamp;

@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlRootElement(name = "orderBook")
public class OrderBookDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlTransient
	private Long dtoSysOrderbookPk;

	@XmlElement(name = "LastToucedUser")
	private String dtoSysLasttouceduser;

	@XmlElement(name = "LastTouchedTime")
	private Timestamp dtoSysLasttouched;

	@XmlElement(name = "Aspen")
	private AgiTypeDto dtoAspen;

	@XmlElement(name = "Customer")
	private CustTypeDto dtoCustomer;

	@XmlElement(name = "Item")
	private ItemTypeDto dtoItems;

	@XmlElement(name = "Order")
	private OrderTypeDto dtoOrder;

	public OrderBookDto() {
		super();
		this.dtoAspen = new AgiTypeDto();
		this.dtoCustomer = new CustTypeDto();
		this.dtoItems = new ItemTypeDto();
		this.dtoOrder = new OrderTypeDto();
	} // OrderBookDto default constructor

	// Getters & Setters

	public OrderBookDto(OrderBook orderBook) {

		super();
		this.dtoAspen = new AgiTypeDto();
		this.dtoCustomer = new CustTypeDto();
		this.dtoItems = new ItemTypeDto();
		this.dtoOrder = new OrderTypeDto();

		this.setDtoSysOrderbookPk(orderBook.getJpaSysOrderbookPk());

		this.setDtoAspen(orderBook);
		this.setDtoCustomer(orderBook);
		this.setDtoItems(orderBook);
		this.setDtoOrder(orderBook);

		this.setDtoSysLasttoucheduser(orderBook.getJpaSysLasttoucheduser());
		this.setDtoSysLasttouched(orderBook.getJpaSysLasttouched());

	} // OrderBookDto constructor with JPA default from OrderBook

	public OrderBookDto(OrderBookV1 orderBookV1) {

		super();

		this.dtoAspen = new AgiTypeDto();
		this.dtoCustomer = new CustTypeDto();
		this.dtoItems = new ItemTypeDto();
		this.dtoOrder = new OrderTypeDto();

		this.setDtoSysOrderbookPk(0L);

		this.setDtoAspen(orderBookV1);
		this.setDtoCustomer(orderBookV1);
		this.setDtoItems(orderBookV1);
		this.setDtoOrder(orderBookV1);

		this.setDtoSysLasttoucheduser(orderBookV1.getLasttouceduser());
		this.setDtoSysLasttouched(orderBookV1.getLasttouched());

	} // OrderBookDto constructor with OrderBookV1 data.

	public Long getDtoSysOrderbookPk() {
		return dtoSysOrderbookPk;
	}

	public void setDtoSysOrderbookPk(Long dtoSysOrderbookPk) {
		this.dtoSysOrderbookPk = dtoSysOrderbookPk;
	}

	public String getDtoSysLasttoucheduser() {
		return dtoSysLasttouceduser;
	}

	public void setDtoSysLasttoucheduser(String dtoSysLasttouceduser) {
		this.dtoSysLasttouceduser = dtoSysLasttouceduser;
	}

	public Timestamp getDtoSysLasttouched() {
		return dtoSysLasttouched;
	}

	public void setDtoSysLasttouched(Timestamp dtoSysLasttouched) {
		this.dtoSysLasttouched = dtoSysLasttouched;
	}

	public AgiTypeDto getDtoAspen() {
		return dtoAspen.getDtoAgiValues();
	}

	public void setDtoAspen() {
		dtoAspen = new AgiTypeDto();
	}

	public void setDtoAspen(AgiTypeDto atd) {
		if(dtoAspen == null)
			dtoAspen = new AgiTypeDto();
		dtoAspen = atd;
	}

	public void setDtoAspen(OrderBook orderBook) {
		if(dtoAspen == null)
			dtoAspen = new AgiTypeDto();
		dtoAspen.setDtoAgiValues(orderBook);
	}

	public void setDtoAspen(OrderBookQue.DataArea.OrderBook odq) {
		dtoAspen.setDtoAgiValues(odq);
	}

	public void setDtoAspen(OrderBookV1 orderBookV1) {
		if(dtoAspen == null)
			dtoAspen = new AgiTypeDto();
		dtoAspen.setDtoAgiValues(orderBookV1);
	}

	public CustTypeDto getDtoCustomer() {
		return dtoCustomer.getDtoCustValues();
	}

	public void setDtoCustomer(OrderBook orderBook) {
		if(dtoCustomer == null)
			dtoCustomer = new CustTypeDto();
		dtoCustomer.setDtoCustValues(orderBook);
	}

	public void setDtoCustomer(OrderBookQue.DataArea.OrderBook odq) {
		dtoCustomer.setDtoCustValues(odq);
	}

	public void setDtoCustomer(CustTypeDto ctd) {
		if(dtoCustomer == null)
			dtoCustomer = new CustTypeDto();
		dtoCustomer = ctd;
	}

	public void setDtoCustomer(OrderBookV1 obv1) {
		if(dtoCustomer == null)
			dtoCustomer = new CustTypeDto();
		dtoCustomer.setDtoCustValues(obv1);
	}

	public ItemTypeDto getDtoItems() {
		return dtoItems.getDtoItemValues();
	}

	public void setDtoItems(OrderBookQue.DataArea.OrderBook odq) {
		if(dtoItems == null)
			dtoItems = new ItemTypeDto();
		dtoItems.setDtoItemValues(odq);
	}

	public void setDtoItems(OrderBook orderBook) {
		if(dtoItems == null)
			dtoItems = new ItemTypeDto();
		dtoItems.setDtoItemValues(orderBook);
	}

	public void setDtoItems(OrderBookV1 obv1) {
		if(dtoItems == null)
			dtoItems = new ItemTypeDto();
		dtoItems.setDtoItemValues(obv1);
	}

	public void setDtoItems(ItemTypeDto itd) {
		if(dtoItems == null)
			dtoItems = new ItemTypeDto();
		dtoItems = itd;
	}

	public OrderTypeDto getDtoOrder() {
		return dtoOrder.getDtoOrderValues();
	}

	public void setDtoOrder(OrderBookQue.DataArea.OrderBook odq) {
		if(dtoOrder == null)
			dtoOrder = new OrderTypeDto();
		dtoOrder.setDtoOrderValues(odq);
	}

	public void setDtoOrder(OrderBook orderBook) {
		if(dtoOrder == null)
			dtoOrder = new OrderTypeDto();
		dtoOrder.setDtoOrderValues(orderBook);
	}

	public void setDtoOrder(OrderBookV1 odv1) {
		if(dtoOrder == null)
			dtoOrder = new OrderTypeDto();
		dtoOrder.setDtoOrderValues(odv1);
	}

	public void setDtoOrder(OrderTypeDto otd) {
		if(dtoOrder == null)
			dtoOrder = new OrderTypeDto();
		dtoOrder = otd;
	}

	public String getHeaders() {
		return ""
				+ "Item Code;"
				+ "Description;"
				+ "Signal Code;"
				+ "Item Group;"
				+ "Brand Number;"
				+ "Brand Description;"
				+ "Supplier Node;"
				+ "Item Time Fence;"
				+ "QA and Safety Time;"
				+ "Customer Name;"
				+ "Customer Code;"
				+ "End Market Country;"
				+ "Customer PO;"
				+ "Sales Order Number;"
				+ "Sales Order Position;"
				+ "Sales Order Quantity;"
				+ "SKII Code;"
				+ "Busines Unit Number;"
				+ "Busines Unit Description;"
				+ "SO Planned Delivery Date;"
				+ "FPA;"
				+ "COR Number;"
				+ "COR Planned Delivery Date;"
				+ "Ex Site Date;"
				+ "COR Planned Quantity;"
				+ "COR Status;"
				+ "Variance Percentage";
//		+ "AGI Contact"
//		+ "Comments From Market;"
//		+ "Country;"
//		+ "Customer Number;"
//		+ "Email Address Of AGI Contact;"
//		+ "End Customer Name;"
//		+ "End Customer;"
//		+ "Invoice Type;"
//		+ "InvoiceNumber;"
//		+ "Item Group Description;"
//		+ "Manager;"
//		+ "Order Currency;"
//		+ "Order Price;"
//		+ "Order Value;"
//		+ "Original Delivery Date;"
//		+ "Portfolio;"
//		+ "Purchase Order Line;"
//		+ "Purchase Order;"
//		+ "Recognised Quantity;"
//		+ "Revised In Market Date;"
//		+ "Sales Order Date;"
//		+ "Supplier Description;"
//		+ "Supplier Item Code;"
//		+ "Total Lead Time;"

	}


//	@Override
//	public String toString() {
//
//		String outval = ""
//				+ getAgiItemCode()+ ";"
//				+ getItemDescription()+ ";"
//				+ getRhSignalCode() + ";"
//				+ getItemGroup()+ ";"
//				+ getBrand() + ";"
//				+ getBrandDescription()+ ";"
//				+ getSupplierNode() + ";"
//				+ getItemTimeFenceDays()+ ";"
//				+ getQaSafetyTime() + ";"
//				+ getCustomer() + ";"
//				+ getCustomerNode()+ ";"
//				+ getEndMarket()+ ";"
//				+ getCustPo()+ ";"
//				+ getSalesOrder()+ ";"
//				+ getSalesOrderLine() + ";"
//				+ getSalesOrderQuantity()+ ";"
//				+ getSkiiCode()+ ";"
//				+ getBusinesUnit()+ ";"
//				+ getCommentsFromAgi() + ";"
//				+ formatDate(soPlannedDeliveryDate) + ";"
//				+ getFpaReference()+ ";"
//				+ getAgiCorNum()+ ";"
//				+ formatDate(corPlannedDeliveryDate)+ ";"
//				+ formatDate(exSiteDate) + ";"
//				+ getCorQuantity() + ";"
//				+ getCorStatus()+ ";"
//				+ getVariancePercentage();
////				+ getCommentsFromMarket()+ ";"
////				+ getCountry()+ ";"
////				+ getCustomerNumber()+ ";"
////				+ getEmailAddressOfAgiContact() + ";"
////				+ getEndCustomerName()+ ";"
////				+ getEndCustomer()+ ";"
////				+ getInvoiceType() + ";"
////				+ getInvoiceNumber()+ ";"
////				+ getItemGroupDescription() + ";"
////				+ getManager()+ ";"
////				+ getOrderCurrency()+ ";"
////				+ getOrderPrice() + ";"
////				+ getOrderValue()+ ";"
////				+ formatDate(originalDeliveryDate)+ ";"
////				+ getPortfolio() + ";"
////				+ getPurchaseOrderLine()+ ";"
////				+ getPurchaseOrder()+ ";"
////				+ getRecognisedQuantity()+ ";"
////				+ formatDate(revisedInMarketDate)+ ";"
////				+ formatDate(salesOrderDate)+ ";"
////				+ getSupplierDescription()+ ";"
////				+ getSupplierItemCode()+ ";"
////				+ getTotalLeadtime()+ ";"
////				+ getAgiContact() + ";"
//
//
////		System.out.println("Should be added to the CSV:\n" + outval);
//		return outval;
//	}


	@Override
	public String toString() {
		return "OrderBookDto{" +
				"dtoSysOrderbookPk=" + dtoSysOrderbookPk + "\n" +
				", dtoSysLasttouceduser='" + dtoSysLasttouceduser + "\'\n" +
				", dtoSysLasttouched=" + dtoSysLasttouched + "\n" +
				", dtoAspen=" + dtoAspen + "\n" +
				", dtoCustomer=" + dtoCustomer + "\n" +
				", dtoItems=" + dtoItems + "\n" +
				", dtoOrder=" + dtoOrder + "\n" +
				'}';
	}
}
