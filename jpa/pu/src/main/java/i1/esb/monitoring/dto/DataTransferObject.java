package i1.esb.monitoring.dto;



import i1.esb.monitoring.pojo.RsDtoPaging;
import i1.esb.monitoring.pojo.RsDtoUser;

import javax.xml.bind.annotation.*;
import java.util.Date;
import java.util.List;


@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlRootElement(name = "DTO")

public class DataTransferObject {
	
	@XmlElement(name="MESSAGE TYPE")
	private String confType;

	@XmlElement(name="MESSAGE OPERATION")
	private String confOper;

	@XmlElement(name="MESSAGE TIME_IN")
	private Date confTimeIn;

	@XmlElement(name="MESSAGE TIME_OUT")
	private Date confTimeOut;

	@XmlElement(name="MESSAGE RECORDS")
	private Integer confRecCount;

	@XmlElementWrapper (name = "VALUES_LIST")
	private List<NameValueDto> values;

	@XmlElementWrapper (name = "STATUS_LIST")
	private List<NameValueDto> status_list;

	@XmlElementWrapper (name = "END_MARKET_LIST")
	private List<NameValueDto> end_market_list;

	@XmlElementWrapper (name = "ORDER_TYPE_LIST")
	private List<NameValueDto> order_type_list;

	@XmlElementWrapper (name = "CUSTOMER_LIST")
	private List<NameValueDto> customer_list;

	@XmlElementWrapper (name = "DATE_TYPE_LIST")
	private List<NameValueDto> date_type_list;

	@XmlElementWrapper (name = "COMMENT_LIST")
	private List<CommentDto> comment_list;

	@XmlElementWrapper (name = "Page")
	private RsDtoPaging paging;

	@XmlElementWrapper (name = "UserDetails")
	private RsDtoUser userDetails;

	@XmlElementWrapper (name = "MESSAGE_ERRORS")
	private String errorMessage;

	//getters & setters

	@XmlElement(name="MESSAGE TYPE")
	public String getConfType() {
		return confType;
	}

	public void setConfType(String confType) {
		this.confType = confType;
	}

	@XmlElementWrapper (name = "VALUES_LIST" )
	public List<NameValueDto> getValues() {
		return values;
	}

	public void setValues(List<NameValueDto> values) {
		this.values = values;
	}

	@XmlElement(name="MESSAGE OPERATION")
	public String getConfOper() {
		return confOper;
	}

	public void setConfOper(String confOper) {
		this.confOper = confOper;
	}

	@XmlElement(name="MESSAGE TIME_IN")
	public Date getConfTimeIn() {
		return confTimeIn;
	}

	public void setConfTimeIn(Date confTimeIn) {
		this.confTimeIn = confTimeIn;
	}

	@XmlElement(name="MESSAGE TIME_OUT")
	public Date getConfTimeOut() {
		return confTimeOut;
	}

	public void setConfTimeOut(Date confTimeOut) {
		this.confTimeOut = confTimeOut;
	}

	@XmlElement(name="MESSAGE RECORDS")
	public Integer getConfRecCount() {
		return confRecCount;
	}

	public void setConfRecCount(Integer confRecCount) {
		this.confRecCount = confRecCount;
	}

	@XmlElementWrapper (name = "STATUS_LIST" )
	public List<NameValueDto> getStatus_list() {
		return status_list;
	}

	public void setStatus_list(List<NameValueDto> status_list) {
		this.status_list = status_list;
	}

	@XmlElementWrapper (name = "END_MARKET_LIST" )
	public List<NameValueDto> getEnd_market_list() {
		return end_market_list;
	}

	public void setEnd_market_list(List<NameValueDto> end_market_list) {
		this.end_market_list = end_market_list;
	}

	@XmlElementWrapper (name = "ORDER_TYPE_LIST")
	public List<NameValueDto> getOrder_type_list() {
		return order_type_list;
	}

	public void setOrder_type_list(List<NameValueDto> order_type_list) {
		this.order_type_list = order_type_list;
	}

	@XmlElementWrapper (name = "CUSTOMER_LIST")
	public List<NameValueDto> getCustomer_list() {
		return customer_list;
	}

	public void setCustomer_list(List<NameValueDto> customer_list) {
		this.customer_list = customer_list;
	}

	@XmlElementWrapper (name = "DATE_TYPE_LIST")
	public List<NameValueDto> getDate_type_list() {
		return date_type_list;
	}

	public void setDate_type_list(List<NameValueDto> date_type_list) {
		this.date_type_list = date_type_list;
	}

	@XmlElementWrapper (name = "COMMENT_LIST")
	public List<CommentDto> getComment_list() {
		return comment_list;
	}

	public void setComment_list(List<CommentDto> comment_list) {
		this.comment_list = comment_list;
	}

	@XmlElementWrapper (name = "Page")
	public RsDtoPaging getPaging() {
		return paging;
	}

	public void setPaging(RsDtoPaging paging) {
		this.paging = paging;
	}

	@XmlElementWrapper (name = "UserDetails")
	public RsDtoUser getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(RsDtoUser userDetails) {
		this.userDetails = userDetails;
	}

	@XmlElementWrapper (name = "MESSAGE_ERRORS")
	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}


