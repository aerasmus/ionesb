package i1.esb.monitoring.pojo;

import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;

public class RsDtoComment implements Serializable {

    private static final long serialVersionUID = 1L;

    @XmlElement( name = "Rq_Orderbook_Pk" )
    private Long rqSysOrderbookPk;

    @XmlElement( name = "Rq_Orderbook_Number" )
    private Long rqOrderBookCorNumb;

    @XmlElement( name = "Rq_User_Id" )
    private String rqSysRequestUser;

    @XmlElement( name = "Rq_Comment_Srce" )
    private String rqSourceComment;

    @XmlElement( name = "Rq_Comment_Publ" )
    private Boolean rqCommentPublic;

    @XmlElement( name = "Rq_Comment_Text" )
    private String rqCommentText;

    public RsDtoComment() {
        super();
    }

    public RsDtoComment(Long rqSysOrderbookPk, Long rqOrderBookCorNumb, String rqSysRequestUser, String rqSourceComment, Boolean rqCommentPublic, String rqCommentText) {
        this.rqSysOrderbookPk = rqSysOrderbookPk;
        this.rqOrderBookCorNumb = rqOrderBookCorNumb;
        this.rqSysRequestUser = rqSysRequestUser;
        this.rqSourceComment = rqSourceComment;
        this.rqCommentPublic = rqCommentPublic;
        this.rqCommentText = rqCommentText;
    }

    @XmlElement( name = "Rq_Orderbook_Pk" )
    public Long getRqSysOrderbookPk() {
        return rqSysOrderbookPk;
    }

    public void setRqSysOrderbookPk(Long rqSysOrderbookPk) {
        this.rqSysOrderbookPk = rqSysOrderbookPk;
    }

    @XmlElement( name = "Rq_Orderbook_Number" )
    public Long getRqOrderBookCorNumb() {
        return rqOrderBookCorNumb;
    }

    @XmlElement( name = "Rq_User_Id" )
    public String getRqSysRequestUser() {
        return rqSysRequestUser;
    }

    @XmlElement( name = "Rq_Comment_Srce" )
    public String getRqSourceComment() {
        return rqSourceComment;
    }

    @XmlElement( name = "Rq_Comment_Publ" )
    public Boolean getRqCommentPublic() {
        return rqCommentPublic;
    }

    @XmlElement( name = "Rq_Comment_Text" )
    public String getRqCommentText() {
        return rqCommentText;
    }
}
