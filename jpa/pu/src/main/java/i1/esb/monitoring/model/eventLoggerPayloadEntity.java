package i1.esb.monitoring.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table( name = "IIB_EVENT_LOGGER_PAYLOAD", schema = "ESB_SERVICE", catalog = "" )
public class eventLoggerPayloadEntity {
    private String mqmdmsgid;

    @Id
    private String localtransactionid;
    private String globaltransactionid;
    private String bitstreamType;
    private String bitstream;

    @Basic
    @Column( name = "MQMDMSGID" )
    public String getMqmdmsgid() {
        return mqmdmsgid;
    }

    public void setMqmdmsgid(String mqmdmsgid) {
        this.mqmdmsgid = mqmdmsgid;
    }

    @Basic
    @Column( name = "LOCALTRANSACTIONID" )
    public String getLocaltransactionid() {
        return localtransactionid;
    }

    public void setLocaltransactionid(String localtransactionid) {
        this.localtransactionid = localtransactionid;
    }

    @Basic
    @Column( name = "GLOBALTRANSACTIONID" )
    public String getGlobaltransactionid() {
        return globaltransactionid;
    }

    public void setGlobaltransactionid(String globaltransactionid) {
        this.globaltransactionid = globaltransactionid;
    }

    @Basic
    @Column( name = "BITSTREAM_TYPE" )
    public String getBitstreamType() {
        return bitstreamType;
    }

    public void setBitstreamType(String bitstreamType) {
        this.bitstreamType = bitstreamType;
    }

    @Basic
    @Column( name = "BITSTREAM" )
    public String getBitstream() {
        return bitstream;
    }

    public void setBitstream(String bitstream) {
        this.bitstream = bitstream;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        eventLoggerPayloadEntity that = (eventLoggerPayloadEntity) o;
        return Objects.equals(mqmdmsgid, that.mqmdmsgid) &&
                Objects.equals(localtransactionid, that.localtransactionid) &&
                Objects.equals(globaltransactionid, that.globaltransactionid) &&
                Objects.equals(bitstreamType, that.bitstreamType) &&
                Objects.equals(bitstream, that.bitstream);
    }

    @Override
    public int hashCode() {

        return Objects.hash(mqmdmsgid, localtransactionid, globaltransactionid, bitstreamType, bitstream);
    }
}
