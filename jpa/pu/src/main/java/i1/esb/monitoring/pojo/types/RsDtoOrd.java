package i1.esb.monitoring.pojo.types;

import biz.aspenconnect.orderbook.dto.types.OrderTypeDto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlRootElement(name = "Orders")
public class RsDtoOrd implements Serializable {

    private static final long serialVersionUID = 1L;

    @XmlElement(name = "CustomerPoLine")
    private Long dtoOrdCustomerPoLine;
    @XmlElement(name = "FpaReference")
    private String dtoOrdFpaReference;
    @XmlElement(name = "RevisedCorPlanDelDate")
    private Date dtoOrdRevisedCorPlanDelDate;
    @XmlElement(name = "SoCurrency")
    private String dtoOrdSoCurrency;
    @XmlElement(name = "SoDate")
    private Date dtoOrdSoDate;
    @XmlElement(name = "SoLine")
    private Long dtoOrdSoLine;
    @XmlElement(name = "SoNumb")
    private String dtoOrdSoNumb;
    @XmlElement(name = "SoPlannedDeliveryDate")
    private Date dtoOrdSoPlannedDeliveryDate;
    @XmlElement(name = "SoPrice")
    private Double dtoOrdSoPrice;
    @XmlElement(name = "SoQuantity")
    private Double dtoOrdSoQuantity;
    @XmlElement(name = "SoValue")
    private Double dtoOrdSoValue;
    //Todo: Need to get this from ServiceDTO...
    @XmlElement(name = "NumberOfCRFs")
    private Integer dtoOrdNumbCRFs;

    public RsDtoOrd(OrderTypeDto odbDtoOrd) {
        this.dtoOrdCustomerPoLine = odbDtoOrd.getDtoOrdCustomerPoLine();
        this.dtoOrdFpaReference = odbDtoOrd.getDtoOrdFpaReference();
        this.dtoOrdRevisedCorPlanDelDate = odbDtoOrd.getDtoOrdRevisedCorPlanDelDate();
        this.dtoOrdSoCurrency = odbDtoOrd.getDtoOrdSoCurrency();
        this.dtoOrdSoDate = odbDtoOrd.getDtoOrdSoDate();
        this.dtoOrdSoLine = odbDtoOrd.getDtoOrdSoLine();
        this.dtoOrdSoNumb = odbDtoOrd.getDtoOrdSoNumb();
        this.dtoOrdSoPlannedDeliveryDate = odbDtoOrd.getDtoOrdSoPlannedDeliveryDate();
        this.dtoOrdSoPrice = odbDtoOrd.getDtoOrdSoPrice();
        this.dtoOrdSoQuantity = odbDtoOrd.getDtoOrdSoQuantity();
        this.dtoOrdSoValue = odbDtoOrd.getDtoOrdSoValue();
        this.dtoOrdNumbCRFs = odbDtoOrd.getDtoOrdNumbCRF();
    }

    @XmlElement(name = "CustomerPoLine")
    public Long getDtoOrdCustomerPoLine() {
        return dtoOrdCustomerPoLine;
    }

    @XmlElement(name = "FpaReference")
    public String getDtoOrdFpaReference() {
        return dtoOrdFpaReference;
    }

    @XmlElement(name = "RevisedCorPlanDelDate")
    public Date getDtoOrdRevisedCorPlanDelDate() {
        return dtoOrdRevisedCorPlanDelDate;
    }

    @XmlElement(name = "SoCurrency")
    public String getDtoOrdSoCurrency() {
        return dtoOrdSoCurrency;
    }

    @XmlElement(name = "SoDate")
    public Date getDtoOrdSoDate() {
        return dtoOrdSoDate;
    }

    @XmlElement(name = "SoLine")
    public Long getDtoOrdSoLine() {
        return dtoOrdSoLine;
    }

    @XmlElement(name = "SoNumb")
    public String getDtoOrdSoNumb() {
        return dtoOrdSoNumb;
    }

    @XmlElement(name = "SoPlannedDeliveryDate")
    public Date getDtoOrdSoPlannedDeliveryDate() {
        return dtoOrdSoPlannedDeliveryDate;
    }

    @XmlElement(name = "SoPrice")
    public Double getDtoOrdSoPrice() {
        return dtoOrdSoPrice;
    }

    @XmlElement(name = "SoQuantity")
    public Double getDtoOrdSoQuantity() {
        return dtoOrdSoQuantity;
    }

    @XmlElement(name = "SoValue")
    public Double getDtoOrdSoValue() {
        return dtoOrdSoValue;
    }

    @XmlElement(name = "NumberOfCRFs")
    public Integer getDtoOrdNumbCRFs() {
        return dtoOrdNumbCRFs;
    }

    @Override
    public String toString() {
        return "RsDtoOrd{" +
                "dtoOrdFpaReference='" + dtoOrdFpaReference + '\'' +
                ", dtoOrdRevisedCorPlanDelDate=" + dtoOrdRevisedCorPlanDelDate +
                ", dtoOrdSoCurrency='" + dtoOrdSoCurrency + '\'' +
                ", dtoOrdSoDate=" + dtoOrdSoDate +
                ", dtoOrdSoLine=" + dtoOrdSoLine +
                ", dtoOrdSoNumb='" + dtoOrdSoNumb + '\'' +
                ", dtoOrdSoPlannedDeliveryDate=" + dtoOrdSoPlannedDeliveryDate +
                ", dtoOrdSoPrice=" + dtoOrdSoPrice +
                ", dtoOrdSoQuantity=" + dtoOrdSoQuantity +
                ", dtoOrdSoValue=" + dtoOrdSoValue +
                ", dtoOrdNumbCRFs=" + dtoOrdNumbCRFs +
                '}';
    }
}
