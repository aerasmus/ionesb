package i1.esb.monitoring.dto;

import biz.aspenconnect.orderbook.model.OrderBookCmnt;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlRootElement(name = "Comment")
public class CommentDto {

    public CommentDto() {
        super();
    }

    public CommentDto(OrderBookCmnt odbc) {
        this.dtoCommentSource = odbc.getJpaCommentSource();
        this.dtoCommentId = odbc.getJpaCommentId();
        this.dtoDomainUserId = odbc.getJpaDomainUserId();
        this.dtoUserName = odbc.getJpaUserName();
        this.dtoCommentTimestamp = odbc.getJpaCommentTimestamp();
        this.dtoComment = odbc.getJpaComment();
        this.dtoPublicComment = odbc.isJpaPublicComment();
    }

    @XmlElement(name = "CommentSource")
    private String dtoCommentSource;

    @XmlElement(name = "CommentId")
    private long dtoCommentId;

    @XmlElement(name = "DomainUserId")
    private String dtoDomainUserId;

    @XmlElement(name = "UserName")
    private String dtoUserName;

    @XmlElement(name = "CommentTimestamp")
    private Date dtoCommentTimestamp;

    @XmlElement(name = "Comment")
    private String dtoComment;

    @XmlElement(name = "PublicComment")
    private boolean dtoPublicComment;



}
