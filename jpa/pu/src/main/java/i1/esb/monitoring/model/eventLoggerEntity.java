package i1.esb.monitoring.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table( name = "IIB_EVENT_LOGGER", schema = "ESB_SERVICE", catalog = "" )
public class eventLoggerEntity {
    private String broker;
    private String executiongroup;
    private String messageflow;
    @Id
    private String eventname;
    private String eventsource;
    @Id
    private String creationtime;
    @Id
    private String localtransactionid;
    private String simplecontentkey1;
    private String simplecontent1;
    private String simplecontentkey2;
    private String simplecontent2;
    private String simplecontentkey3;
    private String simplecontent3;
    private String simplecontentkey4;
    private String simplecontent4;
    private String simplecontentkey5;
    private String simplecontent5;
    private String bitstreamType;
    private String bitstream;

    @Basic
    @Column( name = "BROKER" )
    public String getBroker() {
        return broker;
    }

    public void setBroker(String broker) {
        this.broker = broker;
    }

    @Basic
    @Column( name = "EXECUTIONGROUP" )
    public String getExecutiongroup() {
        return executiongroup;
    }

    public void setExecutiongroup(String executiongroup) {
        this.executiongroup = executiongroup;
    }

    @Basic
    @Column( name = "MESSAGEFLOW" )
    public String getMessageflow() {
        return messageflow;
    }

    public void setMessageflow(String messageflow) {
        this.messageflow = messageflow;
    }

    @Basic
    @Column( name = "EVENTNAME" )
    public String getEventname() {
        return eventname;
    }

    public void setEventname(String eventname) {
        this.eventname = eventname;
    }

    @Basic
    @Column( name = "EVENTSOURCE" )
    public String getEventsource() {
        return eventsource;
    }

    public void setEventsource(String eventsource) {
        this.eventsource = eventsource;
    }

    @Basic
    @Column( name = "CREATIONTIME" )
    public String getCreationtime() {
        return creationtime;
    }

    public void setCreationtime(String creationtime) {
        this.creationtime = creationtime;
    }

    @Basic
    @Column( name = "LOCALTRANSACTIONID" )
    public String getLocaltransactionid() {
        return localtransactionid;
    }

    public void setLocaltransactionid(String localtransactionid) {
        this.localtransactionid = localtransactionid;
    }

    @Basic
    @Column( name = "SIMPLECONTENTKEY1" )
    public String getSimplecontentkey1() {
        return simplecontentkey1;
    }

    public void setSimplecontentkey1(String simplecontentkey1) {
        this.simplecontentkey1 = simplecontentkey1;
    }

    @Basic
    @Column( name = "SIMPLECONTENT1" )
    public String getSimplecontent1() {
        return simplecontent1;
    }

    public void setSimplecontent1(String simplecontent1) {
        this.simplecontent1 = simplecontent1;
    }

    @Basic
    @Column( name = "SIMPLECONTENTKEY2" )
    public String getSimplecontentkey2() {
        return simplecontentkey2;
    }

    public void setSimplecontentkey2(String simplecontentkey2) {
        this.simplecontentkey2 = simplecontentkey2;
    }

    @Basic
    @Column( name = "SIMPLECONTENT2" )
    public String getSimplecontent2() {
        return simplecontent2;
    }

    public void setSimplecontent2(String simplecontent2) {
        this.simplecontent2 = simplecontent2;
    }

    @Basic
    @Column( name = "SIMPLECONTENTKEY3" )
    public String getSimplecontentkey3() {
        return simplecontentkey3;
    }

    public void setSimplecontentkey3(String simplecontentkey3) {
        this.simplecontentkey3 = simplecontentkey3;
    }

    @Basic
    @Column( name = "SIMPLECONTENT3" )
    public String getSimplecontent3() {
        return simplecontent3;
    }

    public void setSimplecontent3(String simplecontent3) {
        this.simplecontent3 = simplecontent3;
    }

    @Basic
    @Column( name = "SIMPLECONTENTKEY4" )
    public String getSimplecontentkey4() {
        return simplecontentkey4;
    }

    public void setSimplecontentkey4(String simplecontentkey4) {
        this.simplecontentkey4 = simplecontentkey4;
    }

    @Basic
    @Column( name = "SIMPLECONTENT4" )
    public String getSimplecontent4() {
        return simplecontent4;
    }

    public void setSimplecontent4(String simplecontent4) {
        this.simplecontent4 = simplecontent4;
    }

    @Basic
    @Column( name = "SIMPLECONTENTKEY5" )
    public String getSimplecontentkey5() {
        return simplecontentkey5;
    }

    public void setSimplecontentkey5(String simplecontentkey5) {
        this.simplecontentkey5 = simplecontentkey5;
    }

    @Basic
    @Column( name = "SIMPLECONTENT5" )
    public String getSimplecontent5() {
        return simplecontent5;
    }

    public void setSimplecontent5(String simplecontent5) {
        this.simplecontent5 = simplecontent5;
    }

    @Basic
    @Column( name = "BITSTREAM_TYPE" )
    public String getBitstreamType() {
        return bitstreamType;
    }

    public void setBitstreamType(String bitstreamType) {
        this.bitstreamType = bitstreamType;
    }

    @Basic
    @Column( name = "BITSTREAM" )
    public String getBitstream() {
        return bitstream;
    }

    public void setBitstream(String bitstream) {
        this.bitstream = bitstream;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        eventLoggerEntity that = (eventLoggerEntity) o;
        return Objects.equals(broker, that.broker) &&
                Objects.equals(executiongroup, that.executiongroup) &&
                Objects.equals(messageflow, that.messageflow) &&
                Objects.equals(eventname, that.eventname) &&
                Objects.equals(eventsource, that.eventsource) &&
                Objects.equals(creationtime, that.creationtime) &&
                Objects.equals(localtransactionid, that.localtransactionid) &&
                Objects.equals(simplecontentkey1, that.simplecontentkey1) &&
                Objects.equals(simplecontent1, that.simplecontent1) &&
                Objects.equals(simplecontentkey2, that.simplecontentkey2) &&
                Objects.equals(simplecontent2, that.simplecontent2) &&
                Objects.equals(simplecontentkey3, that.simplecontentkey3) &&
                Objects.equals(simplecontent3, that.simplecontent3) &&
                Objects.equals(simplecontentkey4, that.simplecontentkey4) &&
                Objects.equals(simplecontent4, that.simplecontent4) &&
                Objects.equals(simplecontentkey5, that.simplecontentkey5) &&
                Objects.equals(simplecontent5, that.simplecontent5) &&
                Objects.equals(bitstreamType, that.bitstreamType) &&
                Objects.equals(bitstream, that.bitstream);
    }

    @Override
    public int hashCode() {

        return Objects.hash(broker, executiongroup, messageflow, eventname, eventsource, creationtime, localtransactionid, simplecontentkey1, simplecontent1, simplecontentkey2, simplecontent2, simplecontentkey3, simplecontent3, simplecontentkey4, simplecontent4, simplecontentkey5, simplecontent5, bitstreamType, bitstream);
    }
}
