package i1.esb.monitoring.pojo;

import biz.aspenconnect.orderbook.dto.OrderBookDto;

import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;

public class RqOrderbook implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement(name = "filterOptions")
	private FilterOptions fo;

	@XmlElement(name = "Page")
	private RsDtoPaging po;

	public RqOrderbook() {
		super();
	}

	public RqOrderbook(FilterOptions fo, RsDtoPaging po) {
		this.fo = fo;
		this.po = po;
	}

	@XmlElement(name = "filterOptions")
	public FilterOptions getFo() {
		return fo;
	}

	public void setFo(FilterOptions fo) {
		this.fo = fo;
	}

	@XmlElement(name = "Page")
	public RsDtoPaging getPo() {
		return po;
	}

	public void setPo(RsDtoPaging po) {
		this.po = po;
	}



}
