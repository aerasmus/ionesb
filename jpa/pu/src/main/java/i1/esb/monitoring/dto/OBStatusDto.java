package i1.esb.monitoring.dto;

import javax.persistence.Column;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Date;

@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlRootElement(name = "OrderBookStatus")
public class OBStatusDto {
	
	@XmlTransient
	@Column(name = "OBSTATUS_PK")
	Long obStatusPk;
	
	@Column(name = "OBSTATUS_NO")	
	Integer obStatusNo;
	
	@Column(name = "OBSTATUS_VAL")	
	String obStatusVal;
	
	@Column(name = "LASTTOUCHED")	
	Date lastTouched;
	
	@Column(name = "LASTTOUCHEDUSER")	
	String lastTouchedUser;

	public OBStatusDto() {
		super();
	}

	public Long getObStatusPk() {
		return obStatusPk;
	}

	public void setObStatusPk(Long obStatusPk) {
		this.obStatusPk = obStatusPk;
	}

	public Integer getObStatusNo() {
		return obStatusNo;
	}

	public void setObStatusNo(Integer obStatusNo) {
		this.obStatusNo = obStatusNo;
	}

	public String getObStatusVal() {
		return obStatusVal;
	}

	public void setObStatusVal(String obStatusVal) {
		this.obStatusVal = obStatusVal;
	}

	public Date getLastTouched() {
		return lastTouched;
	}

	public void setLastTouched(Date lastTouched) {
		this.lastTouched = lastTouched;
	}

	public String getLastTouchedUser() {
		return lastTouchedUser;
	}

	public void setLastTouchedUser(String lastTouchedUser) {
		this.lastTouchedUser = lastTouchedUser;
	}

	@Override
	public String toString() {
		return "OBStatusDto [obStatusPk=" + obStatusPk + ", obStatusNo=" + obStatusNo + ", obStatusVal=" + obStatusVal + ", lastTouched=" + lastTouched + ", lastTouchedUser=" + lastTouchedUser + "]";
	}

	
}
