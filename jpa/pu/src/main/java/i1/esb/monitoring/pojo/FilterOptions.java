package i1.esb.monitoring.pojo;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlRootElement(name="filterOptions")

public class FilterOptions implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String agiCorNum;
	private String company;
	private String orderNumber;
	private Long userPk;
	private String dateStart;
	private String dateEnd;
	private String statusCor;
	private Boolean isInit;
	private String dateType;
	private String customer;
	private Integer fenceTime;
	private String endMarket;
	private String itemCodeCustomer;
	private String itemCode;
	private String itemDescription;
	private String orderType;
	private boolean limit;
	
	// Constructor
	public FilterOptions() {
		super();
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getDateStart() {
		return dateStart;
	}

	public void setDateStart(String dateStart) {
		this.dateStart = dateStart;
	}

	public String getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(String dateEnd) {
		this.dateEnd = dateEnd;
	}

	public String getStatusCor() {
		return statusCor;
	}

	public void setStatusCor(String statusCor) {
		this.statusCor = statusCor;
	}	

	public Boolean getIsInit() {
		return isInit;
	}

	public void setIsInit(Boolean isInit) {
		this.isInit = isInit;
	}

	public String getDateType() {
		return dateType;
	}

	public void setDateType(String dateType) {
		this.dateType = dateType;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public Integer getFenceTime() {
		return fenceTime;
	}

	public void setFenceTime(Integer fenceTime) {
		this.fenceTime = fenceTime;
	}

	public String getAgiCorNum() {
		return agiCorNum;
	}

	public void setAgiCorNum(String agiCorNum) {
		this.agiCorNum = agiCorNum;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public boolean isLimit() {
		return limit;
	}

	public void setLimit(boolean limit) {
		this.limit = limit;
	}

	public Long getUserPk() {
		return userPk;
	}

	public void setUserPk(Long userPk) {
		this.userPk = userPk;
	}

	public String getEndMarket() {
		return endMarket;
	}

	public void setEndMarket(String endMarket) {
		this.endMarket = endMarket;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemCodeCustomer() {
		return itemCodeCustomer;
	}

	public void setItemCodeCustomer(String itemCodeCustomer) {
		this.itemCodeCustomer = itemCodeCustomer;
	}

	public String getItemDescription() {
		return itemDescription;
	}

	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}

	public Boolean getInit() {
		return isInit;
	}

	public void setInit(Boolean init) {
		isInit = init;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	@Override
	public String toString() {
		return "FilterOptions{" +
				"\n                                                                                                             agiCorNum='" + agiCorNum + '\'' +
				", \n                                                                                                             company='" + company + '\'' +
				", \n                                                                                                             orderNumber='" + orderNumber + '\'' +
				", \n                                                                                                             userPk=" + userPk +
				", \n                                                                                                             dateStart='" + dateStart + '\'' +
				", \n                                                                                                             dateEnd='" + dateEnd + '\'' +
				", \n                                                                                                             statusCor='" + statusCor + '\'' +
				", \n                                                                                                             isInit=" + isInit +
				", \n                                                                                                             dateType='" + dateType + '\'' +
				", \n                                                                                                             customer='" + customer + '\'' +
				", \n                                                                                                             fenceTime=" + fenceTime +
				", \n                                                                                                             endMarket='" + endMarket + '\'' +
				", \n                                                                                                             itemCode='" + itemCode + '\'' +
				", \n                                                                                                             itemCodeCustomer='" + itemCodeCustomer + '\'' +
				", \n                                                                                                             itemDescription='" + itemDescription + '\'' +
				", \n                                                                                                             orderType='" + orderType + '\'' +
				", \n                                                                                                             limit=" + limit +
				"\n                                                                                                             }";
	}
}
