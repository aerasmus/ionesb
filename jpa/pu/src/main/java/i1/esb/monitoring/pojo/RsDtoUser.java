package i1.esb.monitoring.pojo;

import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;

public class RsDtoUser implements Serializable {

    /*
     The structure of this object was taken from the @swimlane/ngx-datatables implementtion for server side paging.
     */

    private static final long serialVersionUID = 1L;

    @XmlElement( name = "userPk" )
    private Long userPk;

    @XmlElement( name = "username" )
    private String username;

    @XmlElement( name = "firstname" )
    private String firstname;

    @XmlElement( name = "lastname" )
    private String lastname;

    @XmlElement( name = "email" )
    private String email;

    @XmlElement( name = "selected_company" )
    private Integer selected_company;

    @XmlElement( name = "isInternal" )
    private boolean isInternal;

    public RsDtoUser() {
        super();
    }

    @XmlElement( name = "userPk" )
    public Long getUserPk() {
        return userPk;
    }

    public void setUserPk(Long userPk) {
        this.userPk = userPk;
    }

    @XmlElement( name = "username" )
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @XmlElement( name = "firstname" )
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @XmlElement( name = "lastname" )
    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @XmlElement( name = "email" )
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @XmlElement( name = "selected_company" )
    public Integer getSelected_company() {
        return selected_company;
    }

    public void setSelected_company(Integer selected_company) {
        this.selected_company = selected_company;
    }

    @XmlElement( name = "isInternal" )
    public boolean isInternal() {
        return isInternal;
    }

    public void setInternal(boolean internal) {
        isInternal = internal;
    }
}

