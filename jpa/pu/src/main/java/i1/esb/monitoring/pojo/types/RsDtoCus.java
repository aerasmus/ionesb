package i1.esb.monitoring.pojo.types;

import biz.aspenconnect.orderbook.dto.types.CustTypeDto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlRootElement(name = "Customer")
public class RsDtoCus implements Serializable {

    private static final long serialVersionUID = 1L;

    @XmlElement(name = "BusinesUnitDesc")
    private String dtoCusBusinesUnitDesc;
    @XmlElement(name = "BusinesUnitNumb")
    private String dtoCusBusinesUnitNumb;
    @XmlElement(name = "CustomerCode")
    private String dtoCusCustomerCode;
    @XmlElement(name = "CustomerItemCode")
    private String dtoCusCustomerItemCode;
    @XmlElement(name = "CustomerName")
    private String dtoCusCustomerName;
    @XmlElement(name = "CustomerNumber")
    private String dtoCusCustomerNumber;
    @XmlElement(name = "EndMarketCustCode")
    private String dtoCusEndMarketCustCode;
    @XmlElement(name = "EndMarketCustCtry")
    private String dtoCusEndMarketCustCtry;
    @XmlElement(name = "EndMarketCustName")
    private String dtoCusEndMarketCustName;
    @XmlElement(name = "PoNumb")
    private String dtoCusPoNumb;

    public RsDtoCus(CustTypeDto odbDtoCus) {
        this.dtoCusBusinesUnitDesc = odbDtoCus.getDtoCusBusinesUnitDesc();
        this.dtoCusBusinesUnitNumb = odbDtoCus.getDtoCusBusinesUnitNumb();
        this.dtoCusCustomerCode = odbDtoCus.getDtoCusCustomerCode();
        this.dtoCusCustomerItemCode = odbDtoCus.getDtoCusCustomerItemCode();
        this.dtoCusCustomerName = odbDtoCus.getDtoCusCustomerName();
        this.dtoCusCustomerNumber = odbDtoCus.getDtoCusCustomerNumber();
        this.dtoCusEndMarketCustCode = odbDtoCus.getDtoCusEndMarketCustCode();
        this.dtoCusEndMarketCustCtry = odbDtoCus.getDtoCusEndMarketCustCtry();
        this.dtoCusEndMarketCustName = odbDtoCus.getDtoCusEndMarketCustName();
        this.dtoCusPoNumb = odbDtoCus.getDtoCusPoNumb();
    }

    @XmlElement(name = "BusinesUnitDesc")
    public String getDtoCusBusinesUnitDesc() {
        return dtoCusBusinesUnitDesc;
    }

    @XmlElement(name = "BusinesUnitNumb")
    public String getDtoCusBusinesUnitNumb() {
        return dtoCusBusinesUnitNumb;
    }

    @XmlElement(name = "CustomerCode")
    public String getDtoCusCustomerCode() {
        return dtoCusCustomerCode;
    }

    public String getDtoCusCustomerItemCode() {
        return dtoCusCustomerItemCode;
    }

    @XmlElement(name = "CustomerName")
    public String getDtoCusCustomerName() {
        return dtoCusCustomerName;
    }

    @XmlElement(name = "CustomerNumber")
    public String getDtoCusCustomerNumber() {
        return dtoCusCustomerNumber;
    }

    @XmlElement(name = "EndMarketCustCode")
    public String getDtoCusEndMarketCustCode() {
        return dtoCusEndMarketCustCode;
    }

    @XmlElement(name = "EndMarketCustCtry")
    public String getDtoCusEndMarketCustCtry() {
        return dtoCusEndMarketCustCtry;
    }

    @XmlElement(name = "EndMarketCustName")
    public String getDtoCusEndMarketCustName() {
        return dtoCusEndMarketCustName;
    }

    @XmlElement(name = "PoNumb")
    public String getDtoCusPoNumb() {
        return dtoCusPoNumb;
    }

    @Override
    public String toString() {
        return "RsDtoCus{" +
                "dtoCusBusinesUnitDesc='" + dtoCusBusinesUnitDesc + '\'' +
                ", dtoCusBusinesUnitNumb='" + dtoCusBusinesUnitNumb + '\'' +
                ", dtoCusCustomerCode='" + dtoCusCustomerCode + '\'' +
                ", dtoCusCustomerItemCode='" + dtoCusCustomerItemCode + '\'' +
                ", dtoCusCustomerName='" + dtoCusCustomerName + '\'' +
                ", dtoCusCustomerNumber='" + dtoCusCustomerNumber + '\'' +
                ", dtoCusEndMarketCustCode='" + dtoCusEndMarketCustCode + '\'' +
                ", dtoCusEndMarketCustCtry='" + dtoCusEndMarketCustCtry + '\'' +
                ", dtoCusEndMarketCustName='" + dtoCusEndMarketCustName + '\'' +
                ", dtoCusPoNumb='" + dtoCusPoNumb + '\'' +
                '}';
    }
}
