package i1.esb.monitoring.pojo;

import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;

public class RsDtoPaging implements Serializable {

    /*
     The structure of this object was taken from the @swimlane/ngx-datatables implementtion for server side paging.
     */

    private static final long serialVersionUID = 1L;

    //The current page number
    @XmlElement( name = "pageNumber" )
    private Integer pageNumber;

    //The number of elements in the page
    @XmlElement( name = "size" )
    private Integer pageSize;

    //The total number of elements
    @XmlElement( name = "totalElements" )
    private Integer totalElements;

    //The total number of pages
    @XmlElement( name = "totalPages" )
    private Integer totalPages;


    public RsDtoPaging() {
        super();
    }

    @XmlElement( name = "pageNumber" )
    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    @XmlElement( name = "size" )
    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    @XmlElement( name = "totalElements" )
    public Integer getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(Integer totalElements) {
        this.totalElements = totalElements;
    }

    @XmlElement( name = "totalPages" )
    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }
}

