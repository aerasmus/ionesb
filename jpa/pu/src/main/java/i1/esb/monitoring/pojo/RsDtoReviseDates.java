package i1.esb.monitoring.pojo;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class RsDtoReviseDates implements Serializable {

    private static final long serialVersionUID = 1L;

    @XmlElement( name = "Rq_Orderbook_Number" )
    private Long rqOrderBookCorNumb;

    @XmlElement( name = "Rq_Revised_ExSite_Date" )
    private Date rqRevisedExSiteDate;

    @XmlElement( name = "Rq_Revised_Plan_Del_Date" )
    private Date rqRevisedDelDate;

    @XmlTransient
    private Timestamp rqRevisedTouched;

    @XmlTransient
    private String rqRevisedUser;

    public RsDtoReviseDates() {
        super();
    }

    public RsDtoReviseDates(Long rqOrderBookCorNumb, Date rqRevisedExSiteDate, Date rqRevisedDelDate, Timestamp rqRevisedTouched, String rqRevisedUser) {
        this.rqOrderBookCorNumb = rqOrderBookCorNumb;
        this.rqRevisedExSiteDate = rqRevisedExSiteDate;
        this.rqRevisedDelDate = rqRevisedDelDate;
        this.rqRevisedTouched = rqRevisedTouched;
        this.rqRevisedUser = rqRevisedUser;
    }

    @XmlElement( name = "Rq_Orderbook_Number" )
    public Long getRqOrderBookCorNumb() {
        return rqOrderBookCorNumb;
    }

    @XmlElement( name = "Rq_Revised_ExSite_Date" )
    public Date getRqRevisedExSiteDate() {
        return rqRevisedExSiteDate;
    }

    @XmlElement( name = "Rq_Revised_Plan_Del_Date" )
    public Date getRqRevisedDelDate() {
        return rqRevisedDelDate;
    }

    public Timestamp getRqRevisedTouched() {
        return rqRevisedTouched;
    }

    public String getRqRevisedUser() {
        return rqRevisedUser;
    }

    public void setRqRevisedTouched(Timestamp rqRevisedTouched) {
        this.rqRevisedTouched = rqRevisedTouched;
    }

    public void setRqRevisedUser(String rqRevisedUser) {
        this.rqRevisedUser = rqRevisedUser;
    }
}
