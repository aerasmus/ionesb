package i1.esb.monitoring.pojo.types;

import biz.aspenconnect.orderbook.dto.types.AgiTypeDto;

import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class RsDtoAgi implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement(name = "Cluster")
	private String dtoAgiCluster;
	@XmlElement(name = "Contact")
	private String dtoAgiContact;
	@XmlElement(name = "ContactEmail")
	private String dtoAgiContactEmail;
	@XmlElement(name = "CorNumb")
	private Long dtoAgiCorNumb;
	@XmlElement(name = "CorStatus")
	private String dtoAgiCorStatus;
	@XmlElement(name = "ExSiteDate")
	private Date dtoAgiExSiteDate;
	@XmlElement(name = "InvoiceNumb")
	private String dtoAgiInvoiceNumb;
	@XmlElement(name = "InvoiceType")
	private String dtoAgiInvoiceType;
	@XmlElement(name = "ChangedDeliveryDate")
	private Date dtoAgiChangedDeliveryDate;
	@XmlElement(name = "ChangedDeliveryTouched")
	private Timestamp dtoAgiChangedDeliveryTouched;
	@XmlElement(name = "OriginalDeliveryDate")
	private Date dtoAgiOriginalDeliveryDate;
	@XmlElement(name = "PlannedDeliveryDate")
	private Date dtoAgiPlannedDeliveryDate;
	@XmlElement(name = "PlannedQuantity")
	private Double dtoAgiPlannedQuantity;
	@XmlElement(name = "RecognisedQuantity")
	private Double dtoAgiRecognisedQuantity;
	@XmlElement(name = "RevisedExSiteDate")
	private Date dtoAgiRevisedExSiteDate;
	@XmlElement(name = "VarianceDeliveryDate")
	private Double dtoAgiVarianceDeliveryDate;
	@XmlElement(name = "AgiVariancePlannedQty")
	private Double dtoAgiVariancePlannedQty;

	public  RsDtoAgi(AgiTypeDto odbDtoAgi) {

		this.dtoAgiCluster = odbDtoAgi.getDtoAgiCluster();
		this.dtoAgiContact = odbDtoAgi.getDtoAgiContact();
		this.dtoAgiContactEmail = odbDtoAgi.getDtoAgiContactEmail();
		this.dtoAgiCorNumb = odbDtoAgi.getDtoAgiCorNumb();
		this.dtoAgiCorStatus = odbDtoAgi.getDtoAgiCorStatus();
		this.dtoAgiExSiteDate = odbDtoAgi.getDtoAgiExSiteDate();
		this.dtoAgiInvoiceNumb = odbDtoAgi.getDtoAgiInvoiceNumb();
		this.dtoAgiInvoiceType = odbDtoAgi.getDtoAgiInvoiceType();
		this.dtoAgiChangedDeliveryDate = odbDtoAgi.getDtoAgiChangedDeliveryDate();
		this.dtoAgiChangedDeliveryTouched = odbDtoAgi.getDtoAgiChangedDeliveryTouched();
		this.dtoAgiOriginalDeliveryDate = odbDtoAgi.getDtoAgiOriginalDeliveryDate();
		this.dtoAgiPlannedDeliveryDate = odbDtoAgi.getDtoAgiPlannedDeliveryDate();
		this.dtoAgiPlannedQuantity = odbDtoAgi.getDtoAgiPlannedQuantity();
		this.dtoAgiRecognisedQuantity = odbDtoAgi.getDtoAgiRecognisedQuantity();
		this.dtoAgiRevisedExSiteDate = odbDtoAgi.getDtoAgiRevisedExSiteDate();
		this.dtoAgiVarianceDeliveryDate = odbDtoAgi.getDtoAgiVarianceDeliveryDate();
		this.dtoAgiVariancePlannedQty = odbDtoAgi.getDtoAgiVariancePlannedQty();
	}
	@XmlElement(name = "Cluster")
	public String getDtoAgiCluster() {
		return dtoAgiCluster;
	}

	@XmlElement(name = "Contact")
	public String getDtoAgiContact() {
		return dtoAgiContact;
	}

	@XmlElement(name = "ContactEmail")
	public String getDtoAgiContactEmail() {
		return dtoAgiContactEmail;
	}

	@XmlElement(name = "CorNumb")
	public Long getDtoAgiCorNumb() {
		return dtoAgiCorNumb;
	}

	@XmlElement(name = "CorStatus")
	public String getDtoAgiCorStatus() {
		return dtoAgiCorStatus;
	}

	@XmlElement(name = "ExSiteDate")
	public Date getDtoAgiExSiteDate() {
		return dtoAgiExSiteDate;
	}

	@XmlElement(name = "InvoiceNumb")
	public String getDtoAgiInvoiceNumb() {
		return dtoAgiInvoiceNumb;
	}

	@XmlElement(name = "InvoiceType")
	public String getDtoAgiInvoiceType() {
		return dtoAgiInvoiceType;
	}

	@XmlElement(name = "ChangedDeliveryDate")
	public Date getDtoAgiChangedDeliveryDate() {
		return dtoAgiChangedDeliveryDate;
	}

	@XmlElement(name = "ChangedDeliveryTouched")
	public Timestamp getDtoAgiChangedDeliveryTouched() {
		return dtoAgiChangedDeliveryTouched;
	}

	@XmlElement(name = "OriginalDeliveryDate")
	public Date getDtoAgiOriginalDeliveryDate() {
		return dtoAgiOriginalDeliveryDate;
	}

	@XmlElement(name = "PlannedDeliveryDate")
	public Date getDtoAgiPlannedDeliveryDate() {
		return dtoAgiPlannedDeliveryDate;
	}

	@XmlElement(name = "PlannedQuantity")
	public Double getDtoAgiPlannedQuantity() {
		return dtoAgiPlannedQuantity;
	}

	@XmlElement(name = "RecognisedQuantity")
	public Double getDtoAgiRecognisedQuantity() {
		return dtoAgiRecognisedQuantity;
	}

	@XmlElement(name = "RevisedExSiteDate")
	public Date getDtoAgiRevisedExSiteDate() {
		return dtoAgiRevisedExSiteDate;
	}

	@XmlElement(name = "VarianceDeliveryDate")
	public Double getDtoAgiVarianceDeliveryDate() {
		return dtoAgiVarianceDeliveryDate;
	}

	@XmlElement(name = "AgiVariancePlannedQty")
	public Double getDtoAgiVariancePlannedQty() {
		return dtoAgiVariancePlannedQty;
	}

	@Override
	public String toString() {
		return "RsDtoAgi{" +
				"dtoAgiCluster='" + dtoAgiCluster + '\'' +
				", dtoAgiContact='" + dtoAgiContact + '\'' +
				", dtoAgiContactEmail='" + dtoAgiContactEmail + '\'' +
				", dtoAgiCorNumb=" + dtoAgiCorNumb +
				", dtoAgiCorStatus='" + dtoAgiCorStatus + '\'' +
				", dtoAgiExSiteDate=" + dtoAgiExSiteDate +
				", dtoAgiInvoiceNumb='" + dtoAgiInvoiceNumb + '\'' +
				", dtoAgiInvoiceType='" + dtoAgiInvoiceType + '\'' +
				", dtoAgiChangedDeliveryDate=" + dtoAgiChangedDeliveryDate +
				", dtoAgiChangedDeliveryTouched=" + dtoAgiChangedDeliveryTouched +
				", dtoAgiOriginalDeliveryDate=" + dtoAgiOriginalDeliveryDate +
				", dtoAgiPlannedDeliveryDate=" + dtoAgiPlannedDeliveryDate +
				", dtoAgiPlannedQuantity=" + dtoAgiPlannedQuantity +
				", dtoAgiRecognisedQuantity=" + dtoAgiRecognisedQuantity +
				", dtoAgiRevisedExSiteDate=" + dtoAgiRevisedExSiteDate +
				", dtoAgiVarianceDeliveryDate=" + dtoAgiVarianceDeliveryDate +
				", dtoAgiVariancePlannedQty=" + dtoAgiVariancePlannedQty +
				'}';
	}
}
