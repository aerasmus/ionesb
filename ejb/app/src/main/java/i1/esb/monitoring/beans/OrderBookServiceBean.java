package i1.esb.monitoring.beans;

import biz.aspenconnect.UserUtils;
import biz.aspenconnect.utils.GeneralUtils;
import i1.esb.monitoring.dto.DataTransferObject;
import i1.esb.monitoring.dto.NameValueDto;
import i1.esb.monitoring.dto.OBStatusDto;
import i1.esb.monitoring.pojo.*;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;
import java.sql.Timestamp;
import java.util.*;
import java.util.logging.Logger;

/**
 * Session Bean implementation class OrderBookService
 */

@Stateless
@LocalBean
public class OrderBookServiceBean {

    public Logger logger = Logger.getLogger(new Object() {
    }.getClass().getName());

    GeneralUtils GU = new GeneralUtils();
    UserUtils UU = new UserUtils();

    @Context
    SecurityContext sec;
    @PersistenceContext( unitName = "EventLoggerModel" )
    private EntityManager em;

    // Constructors
    public OrderBookServiceBean() {
        super();
    }

    private String leadingZero(String fld) {
        return (fld.length() == 1 ? "0" + fld : fld);
    }

    public void addCrfRecord(Long getJpaSysOrderbookPk, Long jpaAgiCorNumb) {
//        ChangeReqForm crf_jpa = new ChangeReqForm();
//
//        crf_jpa.setJpaSysCrfPk(0L);
//        crf_jpa.setJpaSysOrderbookPk(getJpaSysOrderbookPk);
//        crf_jpa.setJpaAgiCorNumb(jpaAgiCorNumb);
//
//        em.persist(crf_jpa);
    }

    public Long countRecs() {

        String query = "SELECT count(*) FROM TRANSACTIONAL.ORDERBOOK_EXT";

        Long result = 0L;
        try {
            result = (Long) em.createNativeQuery(query).getSingleResult();
        } catch (Exception e) {
        }

        return result;

    }

    public long addOrderbookCusItmCode (Long sysOrderbookPk, String customerItmCode){
        logger.info("Checking for PK " + sysOrderbookPk + " to insert Customer Item Code " + customerItmCode);

        if(sysOrderbookPk != null && sysOrderbookPk > 0 && customerItmCode != null && customerItmCode.length() > 0) {

            String query = "SELECT * FROM TRANSACTIONAL.ORDERBOOK_EXT_ITEM_L WHERE SYS_ORDERBOOK_PK = " + sysOrderbookPk;
            List<Object[]> q = em.createNativeQuery(query).getResultList();

            if(q == null || q.size() == 0) {
                query = "INSERT INTO TRANSACTIONAL.ORDERBOOK_EXT_ITEM_L (SYS_ORDERBOOK_PK, CUSTOMER_ITEM_CODE) VALUES (" + sysOrderbookPk + ",'" + customerItmCode + "')";
            } else {
                query = "UPDATE TRANSACTIONAL.ORDERBOOK_EXT_ITEM_L SET CUSTOMER_ITEM_CODE = '" + customerItmCode + "' WHERE SYS_ORDERBOOK_PK = " + sysOrderbookPk;
            }

            Query uquery = em.createNativeQuery(query);
            uquery.executeUpdate();
            logger.info(query);

        }
        return sysOrderbookPk;
    }

    private DataTransferObject getUserDetails(FilterOptions fo) {
        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();

        logger.fine("<DataTransferObject> " + methodName + "(FilterOptions)");

        DataTransferObject dto = new DataTransferObject();

        dto.setConfType("USER_DETAILS");
        dto.setConfOper(methodName);
        dto.setConfTimeIn(new Date());
        dto.setConfRecCount(1);

        RsDtoUser usrDtls = new RsDtoUser();

        if (fo.getUserPk() != null)
            usrDtls.setUserPk(fo.getUserPk());
        if (fo.getCompany() != null)
            usrDtls.setSelected_company(Integer.valueOf(fo.getCompany()));

        usrDtls.setInternal(isInternal(fo));

        dto.setUserDetails(usrDtls);

        return dto;
    }

    public DataTransferObject getCustomersPerCompany(FilterOptions fo) {
        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();

        logger.fine("<DataTransferObject> " + methodName + "(FilterOptions)");

        DataTransferObject dto = new DataTransferObject();

        dto.setConfType("ORDERBOOK_CUSTOMERS");
        dto.setConfOper(methodName);
        dto.setConfTimeIn(new Date());

        dto.setValues(getCustomersByFilter(fo));

        logger.fine("<DataTransferObject> " + methodName + "(FilterOptions) #Records: " + dto.getValues().size());

        dto.setConfRecCount(dto.getValues().size());
        dto.setConfTimeOut(new Date());

        return dto;
    }

    private List<NameValueDto> getCustomersByFilter(FilterOptions fo) {
        List<NameValueDto> cusnvp = new ArrayList<NameValueDto>();

        String query;
        if (fo.getCompany() != null && !fo.getCompany().equals("1"))
            query = "SELECT DISTINCT odb.CUS_CUSTOMER_NAME, odb.CUS_CUSTOMER_CODE FROM TRANSACTIONAL.ORDERBOOK_EXT odb WHERE odb.CUS_BUSINES_UNIT_NUMB  = '"
                    + getBusinessUnitId(Long.parseLong(fo.getCompany())) + "' ORDER BY odb.CUS_CUSTOMER_NAME";
        else
            query = "SELECT DISTINCT odb.CUS_CUSTOMER_NAME, odb.CUS_CUSTOMER_CODE FROM TRANSACTIONAL.ORDERBOOK_EXT odb ORDER BY odb.CUS_CUSTOMER_NAME";

        @SuppressWarnings( "unchecked" )
        List<Object[]> results;
        try {
            results = em.createNativeQuery(query).getResultList();
        } catch (Exception e) {
            results = null;
        }

        results.stream().forEach((columns) -> {
            NameValueDto nobs = new NameValueDto();
            nobs.setName((String) columns[0]);
            nobs.setValue((String) columns[1]);
            cusnvp.add(nobs);
        });
        return cusnvp;
    }

    public DataTransferObject getEndMarketsPerCompany(FilterOptions fo) {
        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();

        logger.fine("<DataTransferObject> " + methodName + "(FilterOptions)");

        DataTransferObject dto = new DataTransferObject();

        dto.setConfType("ORDERBOOK_ENDMARKET");
        dto.setConfOper(methodName);
        dto.setConfTimeIn(new Date());

        List<NameValueDto> cusnvp = new ArrayList<NameValueDto>();

        String query;
        if (fo.getCompany() != null && !fo.getCompany().equals("1"))
            query = "SELECT DISTINCT odb.CUS_END_MARKET_CUST_CTRY FROM TRANSACTIONAL.ORDERBOOK_EXT odb WHERE odb.CUS_BUSINES_UNIT_NUMB = '"
                    + getBusinessUnitId(Long.parseLong(fo.getCompany())) + "' ORDER BY odb.CUS_END_MARKET_CUST_CTRY";
        else
            query = "SELECT DISTINCT odb.CUS_END_MARKET_CUST_CTRY FROM TRANSACTIONAL.ORDERBOOK_EXT odb ORDER BY odb.CUS_END_MARKET_CUST_CTRY";

        @SuppressWarnings( "unchecked" )
        List<String> results = em.createNativeQuery(query).getResultList();

        for (Iterator<String> iterator = results.iterator(); iterator.hasNext(); ) {
            String string = (String) iterator.next();
            NameValueDto nobs = new NameValueDto();
            nobs.setName(string);
            nobs.setValue(string);
            cusnvp.add(nobs);
        }

        dto.setValues(cusnvp);
        dto.setConfRecCount(cusnvp.size());
        dto.setConfTimeOut(new Date());

        logger.fine("<DataTransferObject> " + methodName + "(FilterOptions) #Records: " + cusnvp.size());

        return dto;
    }

    public OBStatusDto getOBStatus(Integer corStatus) {

        logger.fine("List<OBStatusDto> getOBStatus(Integer)");
        String query = "SELECT DISTINCT obs.oBStatusPk, obs.oBDescription FROM REFERENCE.ORDERBOOK_STATUSES obs WHERE obs.OBSTATUS_NO = "
                + corStatus + " ORDER BY obs.oBDescription";
        OBStatusDto status;
        OBStatusDto errStatus = new OBStatusDto();
        errStatus.setObStatusNo(0);
        errStatus.setObStatusVal("Error: No status defined");
        try {
            status = (OBStatusDto) em.createNativeQuery(query).getSingleResult();
        } catch (NoResultException nre) {
            nre.printStackTrace();
            status = errStatus;
        }

        return status;

    }

    public String getOBStatus(String corStatus) {

        String query = "SELECT DISTINCT obs.OBSTATUS_VAL FROM REFERENCE.ORDERBOOK_STATUSES obs WHERE obs.OBSTATUS_NO = "
                + corStatus + " ORDER BY obs.OBSTATUS_VAL FETCH FIRST ROW ONLY";

        String status;
        try {
            status = (String) em.createNativeQuery(query).getSingleResult();
        } catch (Exception e) {
            logger.warning("Error: Status number (" + corStatus + ") does not exist.");
            status = "Error: No status defined";
        }

        return status;

    }

    public DataTransferObject getOBInitConfig(FilterOptions fo) {

        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();

        logger.fine("List<OrderBook> " + methodName + "()");

        DataTransferObject dto = new DataTransferObject();
        // Retrieving the data for the composite call.
        DataTransferObject dtoSts = getOBStatusNVP();
        DataTransferObject dtoCus = getCustomersPerCompany(fo);
        DataTransferObject dtoDte = getOBDateTypesNVP();
        DataTransferObject dtoEmt = getEndMarketsPerCompany(fo);
        DataTransferObject dtoOrd = getOBOrderTypeNVP();
        DataTransferObject dtoUsr = getUserDetails(fo);

        dto.setConfType("ORDERBOOK_CONFIG");
        dto.setConfOper(methodName + "(Composite)");
        dto.setConfTimeIn(new Date());
        dto.setConfRecCount(0);

        // Status List
        if (dtoSts.getValues().size() > 0) {
            dto.setConfRecCount(dto.getConfRecCount() + 1);
            dto.setStatus_list(dtoSts.getValues());
            dto.setConfOper(dto.getConfOper() + "[" + dtoSts.getConfOper() + "|" + dtoSts.getConfRecCount() + "]");
        }
        //End Market List for Company
        if (dtoEmt.getValues().size() > 0) {
            dto.setConfRecCount(dto.getConfRecCount() + 1);
            dto.setEnd_market_list(dtoEmt.getValues());
            dto.setConfOper(dto.getConfOper() + "[" + dtoEmt.getConfOper() + "|" + dtoEmt.getConfRecCount() + "]");
        }
        // Order Type List
        if (dtoOrd.getValues().size() > 0) {
            dto.setConfRecCount(dto.getConfRecCount() + 1);
            dto.setOrder_type_list(dtoOrd.getValues());
            dto.setConfOper(dto.getConfOper() + "[" + dtoOrd.getConfOper() + "|" + dtoOrd.getConfRecCount() + "]");
        }
        // Customer List for Company
        if (dtoCus.getValues().size() > 0) {
            dto.setConfRecCount(dto.getConfRecCount() + 1);
            dto.setCustomer_list(dtoCus.getValues());
            dto.setConfOper(dto.getConfOper() + "[" + dtoCus.getConfOper() + "|" + dtoCus.getConfRecCount() + "]");
        }
        // Date Type List
        if (dtoDte.getValues().size() > 0) {
            dto.setConfRecCount(dto.getConfRecCount() + 1);
            dto.setDate_type_list(dtoDte.getValues());
            dto.setConfOper(dto.getConfOper() + "[" + dtoDte.getConfOper() + "|" + dtoDte.getConfRecCount() + "]");
        }
        // User Details
        if (dtoUsr.getUserDetails() != null) {
            dto.setConfRecCount(dto.getConfRecCount() + 1);
            dto.setUserDetails(dtoUsr.getUserDetails());
            dto.setConfOper(dto.getConfOper() + "[" + dtoDte.getConfOper() + "|" + dtoDte.getConfRecCount() + "]");
        }

        dto.setConfTimeOut(new Date());

        return dto;
    }

    private List<NameValueDto> getNativeQueryNVP(String query) {
        List<NameValueDto> obsnvp = new ArrayList<NameValueDto>();

        @SuppressWarnings( "unchecked" )
        List<Object[]> results = em.createNativeQuery(query).getResultList();

        results.stream().forEach((columns) -> {
            NameValueDto nobs = new NameValueDto();
            nobs.setName((String) columns[0]);
            nobs.setValue((String) columns[1].toString());

            obsnvp.add(nobs);
        });

        return obsnvp;
    }

    public DataTransferObject getOBOrderTypeNVP() {

        String objName = new Object() {
        }.getClass().getEnclosingMethod().getName();

        String query = "SELECT DISTINCT obt.OBTYPES_NAME, obt.OBTYPES_VALUE FROM  REFERENCE.ORDERBOOK_TYPES obt ORDER BY obt.OBTYPES_VALUE";

        logger.fine("<DataTransferObject> " + objName + "()");

        DataTransferObject dto = new DataTransferObject();

        dto.setConfType("ORDERBOOK_ORDER_TYPES");
        dto.setConfOper(objName);
        dto.setConfTimeIn(new Date());

        List<NameValueDto> obsnvp = getNativeQueryNVP(query);

        dto.setValues(obsnvp);
        dto.setConfRecCount(obsnvp.size());
        dto.setConfTimeOut(new Date());

        logger.fine("<DataTransferObject>  " + objName + "() #Records: " + obsnvp.size());

        return dto;

    }

    public DataTransferObject getOBStatusNVP() {

        String objName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        String query = "SELECT DISTINCT obs.OBSTATUS_VAL, obs.OBSTATUS_NO FROM REFERENCE.ORDERBOOK_STATUSES obs ORDER BY obs.OBSTATUS_VAL";

        logger.fine("<DataTransferObject> " + objName + "()");

        DataTransferObject dto = new DataTransferObject();

        dto.setConfType("ORDERBOOK_STATUS");
        dto.setConfOper(new Object() {
        }.getClass().getEnclosingMethod().getName());
        dto.setConfTimeIn(new Date());

        List<NameValueDto> obsnvp = getNativeQueryNVP(query);

        dto.setValues(obsnvp);
        dto.setConfRecCount(obsnvp.size());
        dto.setConfTimeOut(new Date());

        logger.fine("<DataTransferObject>  " + objName + "() #Records: " + obsnvp.size());

        return dto;

    }

    public DataTransferObject getOBDateTypesNVP() {

        String objName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        String query = "SELECT DISTINCT obt.OBDATETYPE_NAME, obt.OBDATETYPE_VAL FROM REFERENCE.ORDERBOOK_DATETYPES obt WHERE obt.OBDATETYPE_USE > 0 ORDER BY obt.OBDATETYPE_NAME";

        logger.fine("<DataTransferObject> " + objName + "()");

        DataTransferObject dto = new DataTransferObject();

        dto.setConfType("ORDERBOOK_DATE_TYPES");
        dto.setConfOper(new Object() {
        }.getClass().getEnclosingMethod().getName());
        dto.setConfTimeIn(new Date());

        List<NameValueDto> obsnvp = getNativeQueryNVP(query);

        dto.setValues(obsnvp);
        dto.setConfRecCount(obsnvp.size());
        dto.setConfTimeOut(new Date());

        logger.fine("<DataTransferObject> " + objName + "() #Records: " + obsnvp.size());

        return dto;
    }

    public List<String[]> getUserAccess(Long user_pk, Long businessEntity_pk) {

        String query = "SELECT ora.CUSTOMERCODE, ora.INTERNALUSER FROM REFERENCE.ORDERBOOKUSERACCESS ora WHERE USER_PK = "
                + user_pk + " AND BUSINESSENTITY_PK = " + businessEntity_pk
                + " AND ACCESSLEVEL IS NOT NULL AND ACCESSLEVEL <> 'N'";

        List<String[]> objResults = new ArrayList<String[]>();

        @SuppressWarnings( "unchecked" )
        List<Object[]> results = (List<Object[]>) em.createNativeQuery(query).getResultList();

        results.stream().forEach((columns) -> {
            String[] nobs = {"", ""};
            nobs[0] = (String) columns[0].toString();
            nobs[1] = (String) columns[1].toString();
            objResults.add(nobs);
        });

        logger.fine("<Query> getUserAccess() :: " + query + "\t\tSize :: " + objResults.size());

        return objResults;
    }

    private boolean isInternal(FilterOptions fo) {

        boolean internal = false;

        List<String[]> custAccess = getUserAccess(fo.getUserPk(), Long.parseLong(fo.getCompany()));

        if (custAccess.size() > 0) {

            internal = true;

            for (String[] strings : custAccess) {
                logger.fine("Returned result :: " + strings[0] + " :: " + strings[1]);
                if (strings[1].equals("0"))
                    internal = false;
            }
        }
        return internal;
    }

    public String getBusinessUnitId(Long businessEntity_pk) {

        String query = "SELECT BU.BUSINESSUNITID FROM REFERENCE.BUSINESSUNIT_BE_LK BBL "
                + "JOIN REFERENCE.BUSINESSUNITS BU ON BU.BUSINESSUNIT_PK = BBL.BUSINESSUINIT_PK "
                + "JOIN APPLICATION.BUSENTITIES BE ON BE.BUSINESSENTITY_PK = BBL.BUSINESSENTITY_PK WHERE BU.BUSINESSENTITY_PK = 1 "
                + "AND BBL.BUSINESSENTITY_PK =  " + businessEntity_pk + " FETCH FIRST ROW ONLY";

        logger.fine("<Query> getBusinessUnitId() :: " + query);

        String result;
        try {
            result = (String) em.createNativeQuery(query).getSingleResult();
        } catch (Exception e) {
            result = "0";
        }

        return result;
    }

    private Integer getOrderBookCount() {
        String query = "SELECT count(*) FROM TRANSACTIONAL.ORDERBOOK_EXT";
        return ((Integer) em.createNativeQuery(query).getSingleResult());
    }

    private Integer getOrderBookCount(String where) {
//        String query = "SELECT count(*) FROM TRANSACTIONAL.ORDERBOOK_EXT ob " + where;
        String query = "SELECT count(*) FROM TRANSACTIONAL.ORDERBOOK_EXT ob LEFT OUTER JOIN TRANSACTIONAL.ORDERBOOK_EXT_ITEM_L oeil ON ob.SYS_ORDERBOOK_PK = oeil.SYS_ORDERBOOK_PK " + where;
        return ((Integer) em.createNativeQuery(query).getSingleResult());
    }

    public String getBusinessUnitDesc(String buid) {

        String query = "SELECT BU.BUSINESSUNIT FROM REFERENCE.BUSINESSUNITS BU WHERE BU.BUSINESSUNITID = '" + buid + "'";

        String result;
        try {
            Query buq = em.createNativeQuery(query);
            logger.fine("getBusinessUnitDesc :: Query :: " + buq.toString());

            result = (String) buq.getResultList().get(0);
        } catch (PersistenceException pe) {
            logger.warning("getBusinessUnitDesc :PersistenceException: " + pe.getMessage());
            pe.printStackTrace();
            result = "";
        } catch (Exception e) {
            logger.warning("getBusinessUnitDesc :Exception: " + e.getMessage());
            e.printStackTrace();
            result = "";
        }
        return result;
    }

}