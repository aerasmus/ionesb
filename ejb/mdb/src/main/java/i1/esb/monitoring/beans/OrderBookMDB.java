package i1.esb.monitoring.beans;

import i1.esb.monitoring.pojo.OrderBookQue;

import javax.annotation.Resource;
import javax.ejb.*;
import javax.jms.*;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import java.io.StringWriter;
import java.util.logging.Logger;

/**
 * Message-Driven Bean implementation class for: OrderBookMDB
 */
@MessageDriven( activationConfig = {
        @ActivationConfigProperty( propertyName = "destination", propertyValue = "OrderBookQ" ),
        @ActivationConfigProperty( propertyName = "destinationType", propertyValue = "javax.jms.Queue" )} )

public class OrderBookMDB implements MessageListener {

    /**
     * Default constructor.
     */

    final Logger logger = Logger.getLogger(OrderBookMDB.class.getName());

//    @EJB
//    OrderBookServiceBean obsb;

    @Resource( name = "CONNFACTORY" ) // inject ConnectionFactory (more)
    private ConnectionFactory factory;
    @Resource( name = "IonesbErrorQ" ) // inject Queue (more)
    private Queue target;

    public OrderBookMDB() {
        super();
    }

    // Sends the given string as text message:
    public void sendMessage(OrderBookQue eOBQ) throws JMSException {
        Connection con = factory.createConnection();

        try {
            Session session = con.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer producer = session.createProducer(target);
            TextMessage outMessage = session.createTextMessage();

            JAXBContext eJc = JAXBContext.newInstance(OrderBookQue.class);

            StringWriter writer = new StringWriter();
            eJc.createMarshaller().marshal(eOBQ, writer);
            outMessage.setText(writer.toString());

            producer.send(outMessage);
            logger.fine("Error message logged to OrderBookErrorQ : "
                    + eOBQ.getApplicationArea().getSender().getReferenceID());
            System.out.println("Message to MQ...");
        } catch (JAXBException jxe) {
            logger.warning("JAXBException : " + jxe.getMessage());
        } catch (JMSException je) {
            logger.warning("JMSException : " + je.getMessage());
        } finally {
            con.close();
        }
    }

    /**
     * @see MessageListener#onMessage(Message)
     * <p>
     * Added this to trap a specific poison message which will be written
     * to an error queue
     */
    @TransactionAttribute( value = TransactionAttributeType.NOT_SUPPORTED )
    public void onMessage(Message message) {

        logger.info("i1 ESB Event Logger Message received");



    }

}
